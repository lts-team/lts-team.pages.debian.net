===================================
Claim/unclaim packages for LTS work
===================================

Information is tracked in ``data/dla-needed.txt`` in the `LTS security tracker`_.

See https://lts-team.pages.debian.net/wiki/Development.html for details.

Unclaiming a package with no CVEs left
======================================

If you claimed a package, and as a result of triaging there are no more open
CVEs for it, instead of unclaiming you can completely remove the package from
``data/dla-needed.txt``.

.. _`LTS security tracker`: https://salsa.debian.org/security-tracker-team/security-tracker
