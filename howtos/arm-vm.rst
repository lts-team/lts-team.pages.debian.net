=================
Create an arm* VM
=================

Create a jessie/stretch/buster-elts armhf VM, to run and debug a
failing package build (or testsuite):

.. code:: bash

  sudo apt install arch-test uidmap binfmt-support \
    qemu-system-arm qemu-user-static

  DIST=buster
  ARCH=armhf
  debvm-create -r $DIST -o $DIST-$ARCH.debvm -k path/to/ssh_id.pub \
    --size=2G -- \
      --architecture=$ARCH \
      --keyring=/publicly-readable/path/to/freexian.gpg \
      http://deb.freexian.com/extended-lts \
      --include=linux-image-5.10-XXX
  # Note: the initramfs step may take several minutes

The part after ``--`` is passed to ``mmdebstrap``.

Replace XXX by the kernel for your target architecture (``armmp`` for
armhf). Using an ELTS kernel backport is required to detect
``/dev/vda`` / ``virtio_blk`` for jessie and stretch.

Optionally append ``--include=strace,vim,...``.

Now run the VM:

.. code:: bash

  debvm-run -i $DIST-$ARCH.debvm --sshport 2222 -- -m 3G

The part after ``--`` is passed to ``qemu``.

Further customizations possible. You can add xxx-lts-proposed. You can
add the target package. ``man debvm-create`` has a few more details.

Limitations:

* The VM is not hardware-accelerated and quite slow (CPU and I/O, by
  an order of magnitude). Using ``eatmydata`` helps a little. Same for
  ``mount -t tmpfs none /mnt/workdir -o size=2G``, but the main
  bottleneck appears to be the CPU more than the disk.

* The VM does not behave exactly like the real buildd hardware,
  different errors may occur.

---------------
Troubleshooting
---------------

* *E: unable to pick chroot mode automatically (use --mode for manual selection)*

Add e.g. ``--mode=unshare`` at the end of the ``debvm-create`` line,
or run through ``sudo``.

* *The following signatures couldn't be verified because the public key is not available: NO_PUBKEY A07310D369055D5A*

Ensure ``freexian.gpg`` is readable by any user on your system
(e.g. not in your home dir).

Also check the output of gpg:

.. code:: bash

  $ gpg --list-key --no-default-keyring --keyring $PWD/freexian-archive-extended-lts.gpg
  /.../freexian-archive-extended-lts.gpg
  -------------------------------------------------------
  pub   rsa4096 2018-05-28 [SC] [expires: 2027-12-05]
        AB597C4F6F3380BD4B2BEBC2A07310D369055D5A
  uid           [ unknown] Extended LTS Repository <sysadmin@freexian.com>

* *passwd: Cannot determine your user name.*

Apply https://salsa.debian.org/helmutg/debvm/-/commit/e4ecc7f745c54a765eff8eff59fc2e9a03371f2e

* init fails to mount disk, ``/dev/vda`` not present (jessie, stretch)

Use a kernel backport: ``--include=linux-image-5.10-armmp`` (armhf) or
``--include=linux-image-5.10-arm64``.

---------------
Other resources
---------------

Pre-built images, with external kernel and initrd:

* https://people.debian.org/~aurel32/qemu/ (squeeze, wheezy)
* https://people.debian.org/~gio/dqib/ (sid)

Manual install from ISO:

* https://quantum5.ca/2020/12/26/how-to-make-an-arm-virtual-machine-armhf-aarch64/ (external kernel and initrd)
* https://quantum5.ca/2022/03/19/how-to-make-better-arm-virtual-machine-armhf-aarch64-uefi/ (UEFI, text-based Debian installer)
