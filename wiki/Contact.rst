***********
Contact
***********

-------------
Mailing lists
-------------

==========
debian-lts
==========

https://lists.debian.org/debian-lts/

**Description:** Discussion and coordination of long-term support work for Debian.
Everyone involved or interested in providing Long-Term Support for Debian should feel free to join this list. Discussion of policy, on-going support issues, and anything else relating to LTS are all on-topic here.

**Subscription policy:** open

==================
debian-lts-changes
==================

https://lists.debian.org/debian-lts-changes/

**Description:** All new package uploads to the LTS ("Long-Term Support") suite are announced here. You should subscribe to this list if you want immediate notification of every package upload to LTS. If you only wish to receive announcements of new package releases on LTS, you may wish to subscribe to the debian-lts-announce mailing list instead.

**Subscription policy:** open

===================
debian-lts-announce
===================

https://lists.debian.org/debian-lts-announce/

**Description:** Important information about new uploads and the status of the LTS distribution are posted here, including security advisories for packages updated in LTS. If you are using the LTS updates on any systems, it is highly recommended that you subscribe to this list, to ensure you are kept fully up-to-date.

**Subscription** policy: open

-----------------------
IRC Channel #debian-lts
-----------------------

IRC Channel: `debian-lts <ircs://irc.oftc.net/debian-lts>`_ (`w/o SSL: debian-lts <irc://irc.oftc.net/debian-lts>`_) (`webchat <https://webchat.oftc.net/?channels=debian-lts>`_) (see `IRC <https://wiki.debian.org/IRC>`_)

We host monthly `Meetings <https://lts-team.pages.debian.net/wiki/Meetings.html>`_.

---------------------------------
Email alias for private exchanges
---------------------------------

The email alias lts-security@debian.org can be used to inform the LTS team of undisclosed vulnerabilities so that security updates can be prepared in advance of their public disclosure. The following persons are behind the email alias:

 * Markus Koschany
 * Thorsten Alteholz
