=====
libav
=====

libav provides a test suite:
https://libav.org/fate.html

The version included in Debian Jessie runs but has lots of errors.
You can run it after *debuild* using:
::

 cd debian-extra-shared/
 # add V=1 for verbose output
 make fate-rsync SAMPLES=/home/build/fate-suite/
 LD_LIBRARY_PATH=libavcodec:libavdevice:libavfilter:libavformat:libavresample:libavutil:libswscale \
   make -k fate \
     SAMPLES=/home/build/fate-suite/ \
   2>&1 | tee tests.log
 grep -c ^TEST tests.log  # 2019-11-05: 1823
 grep -c ^make:.*Error tests.log # 2019-11-05: 141




libav is a fork of ffmpeg.

Bugs may be found by oss-fuzz against the ffmpeg project, which uses a specific in-process fuzzing target such as `target_dec_fuzzer.c <https://github.com/FFmpeg/FFmpeg/blob/master/tools/target_dec_fuzzer.c>`_.

It's not clear if this fuzzing target can be ported to libav to help determine reproduce ffmpeg vulnerabilities in libav.


| Copyright (C) 2019  Sylvain Beucler
