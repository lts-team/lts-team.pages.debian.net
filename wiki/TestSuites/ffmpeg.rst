======
ffmpeg
======

Upstream is making security releases also for old versions as we have
in buster: security updates will be mostly new upstream releases.

On update, check existing Debian-specific patches:

 * remove those that have been adopted upstream
 * send the remaining ones to Michael Niedermayer michael AT niedermayer cc, to vet their inclusion

For new upstream releases, use *7:4.1.10-0+deb10u1* as version number: note the zero.

See also: :doc:`libav` (ffmpeg fork)

-------------
Smoke testing
-------------

This packages has :doc:`autopkgtest` (DEP-8) tests, e.g.:
::

  cd ffmpeg-4.1.1*/
  debi
  # fix-up standard vs. -extra conflicts
  autopkgtest . -- null

One good possible test case is using a ffmpeg-based player, such as
``mplayer`` or ``mpv``, which is operating closely on libavcodec
functions, so one can play a few random files off the internet with it
and the newly built ffmpeg packages.

https://peach.blender.org/ provides files in various video formats
(e.g. https://download.blender.org/peach/bigbuckbunny_movies/ or
https://archive.org/details/BigBuckBunny_124).

A little encoding/decoding never hurts either:
::

  ffmpeg -t 30 -i big_buck_bunny_720p_surround.mp4 -vf scale=320:180 test.ogv


| Copyright (C) 2022  Enrico Zini
| Copyright (C) 2022, 2023  Sylvain Beucler
