===============
libxstream-java
===============

-------------------------
Black list vs. white list
-------------------------

Cf. discussion/analysis at:

 * https://lists.debian.org/debian-lts/2021/06/msg00024.html
 * https://lists.debian.org/debian-lts/2021/06/msg00040.html

----------------------
Smoketest with libjsap
----------------------

This example directly loads a configuration file which really is an app-specific class serialized as XML using XStream:

::

 apt install libjsap-java
 apt source jsap
 cd jsap-2.1/
 cd src/java/
 javac -cp /usr/share/java/xstream.jar:/usr/share/java/jsap-2.1.jar com/martiansoftware/jsap/examples/Manual_HelloWorld_9.java
 cd com/martiansoftware/jsap/examples/
 java -cp /usr/share/java/xstream.jar:/usr/share/java/jsap-2.1.jar:../../../../ com.martiansoftware.jsap.examples.Manual_HelloWorld_9 -n 10 Testing


Other rdeps can be used for testing but are less direct to experiment with

----------------
Simple test file
----------------

With or without white list (commented out):

::

 import com.thoughtworks.xstream.XStream;
 import java.io.*;
 
 public class Basic {
     public static void main(String[] args) {
 	try {
 	    XStream xstream;
 
 	    xstream = new XStream();
	    Person joe = new Person("Joe", "Walnes");
 	    joe.setPhone(new PhoneNumber(123, "1234-456"));
 	    joe.setFax(new PhoneNumber(123, "9999-999"));
 	    String xmlout = xstream.toXML(joe);
 	    System.out.println(xmlout);
 
 	    InputStreamReader xmlin = new InputStreamReader(new FileInputStream(args[0]), "UTF-8");
 	    xstream = new XStream();
 	    //XStream.setupDefaultSecurity(xstream);
 	    //xstream.allowTypes(new Class[] {Person.class});
 	    xstream.alias("person", Person.class);
 	    xstream.alias("phonenumber", PhoneNumber.class);
 	    Person newJoe = (Person)xstream.fromXML(xmlin);
 	    System.out.println(newJoe);
 	    System.out.println(newJoe.phone.code);
 	} catch (IOException e) {
 	    System.out.println(e);
 	}
     }
 }
 
 public class Person {
   private String firstname;
   private String lastname;
   public PhoneNumber phone;
   private PhoneNumber fax;
   public Person(String firstname, String lastname) {
     this.firstname = firstname;
     this.lastname = lastname;
     this.phone = new PhoneNumber(1,"1");
     this.fax = new PhoneNumber(2,"2");
   }
   public void setPhone(PhoneNumber phone) {
     this.phone = phone;
   }
   public void setFax(PhoneNumber fax) {
     this.fax = fax;
   }
 }
 
 public class PhoneNumber {
   public int code;
   public String number;
   public PhoneNumber(int code, String number) {
       this.code = code;
       this.number = number;
   }
 }
 

::

 javac -cp /usr/share/java/xstream.jar Basic.java Person.java PhoneNumber.java 
 java -cp /usr/share/java/xstream.jar:. Basic basic/poc.xml 


| Copyright (C) 2021  Sylvain Beucler
