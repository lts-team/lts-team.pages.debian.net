====
pypy
====

The build requires > 2GB RAM (5GB works), beware if you use a
light VM. Also select the py2 alternative of sphinx
(``update-alternatives --config sphinx-build``).

The testsuite is ran automatically on build, but doesn't fail on
error. Many errors still need to be addressed. Some of the tests fail
if run as root (such as test_unwritable_directory).

To run a single test file before rebuilding/installing a new version:
::

  pypy/goal/pypy-c pypy/test_all.py pypy/module/imp/test/test_import.py
  # the above doesn't recognize module '_struct' for some reason

  # More directly:
  cd lib-python/2.7/
  ../../pypy/goal/pypy-c -m unittest test.test_buffer
  # or if there's a 'main':
  pypy/goal/pypy-c lib-python/2.7/test/test_xmlrpc.py
  pypy/goal/pypy-c lib-python/2.7/test/test_ssl.py

See also
`<https://doc.pypy.org/en/latest/coding-guide.html#testing-modules-in-lib-pypy>`_.

Pypy aims at high compatibily with CPython, including some support for
(recompiled) Python C extensions.  Try and run your favorite Python
scripts with ``pypy``.  Running Debian packages written in Python
often breaks while attempting to load Python C extension not made
available for Pypy, but some do work, e.g.:
::

  PYTHONPATH=/usr/lib/python2.7/dist-packages pypy \
    /usr/bin/txt2tags -t html -o GPL.html /usr/share/common-licenses/GPL

A few leaf packages depend on pypy, make sure to run them:
::

  apt-rdepends -r pypy | less

| Copyright (C) 2025  Sylvain Beucler
