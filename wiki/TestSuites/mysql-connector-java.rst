====================
mysql-connector-java
====================

----------
All suites
----------

The package comes with a testsuite that is disabled by default.

With a few fixes I'll work:
 * src/testsuite/regression/StatementRegressionTest.java: drop testBug33823 (conflict with Debian patches)
 * src/testsuite/regression/StatementRegressionTest.java: *pStmt.setClob(1, null); -> pStmt.setClob(1, (Clob)null);*
 * build.xml: drop *-testsuite-jvm-check* in target *test*

Prepare MySQL server:
 * *apt-get install mysql-server*
 * Enable SSL:
   ::

     cd src/testsuite/ssl-test-certs/
     cp -a ca-cert.pem server-cert.pem server-key.pem /etc/mysql/
     cd /etc/mysql/
     chmod -R go= *.pem
     chown mysql: *.pem

 * */etc/mysql/my.cnf* / */etc/mysql/mariadb.conf.d/50-server.cnf:max_allowed_packet*
   :: 

     # for BlobRegressionTest, simple.ConnectionTest
     max_allowed_packet      = 64M
     innodb_log_file_size    = 640M
     
     ssl-ca=/etc/mysql/ca-cert.pem
     ssl-cert=/etc/mysql/server-cert.pem
     ssl-key=/etc/mysql/server-key.pem

 * *mysql_tzinfo_to_sql /usr/share/zoneinfo | mysql mysql*
 * *CREATE DATABASE testDB;*
 * *GRANT ALL PRIVILEGES ON *.* TO 'root'@'localhost' IDENTIFIED BY 'root';*
 * Stop/start MySQL

If you need to investigate a stuck/long test:
::

        <junit printsummary="yes"
         showoutput="yes" enabletestlistenerevents="true"


Additional build dependencies:
::

 apt-get install libjboss-connector-java  # or grab from wheezy
 cp -a /usr/share/java/junit-3.8.2.jar lib/
 cp -a /usr/share/java/jboss-connector-4.2.3.GA.jar lib/


Old RefFSContextFactory JNDI class that isn't bundled anymore, for *testsuite.simple.DataSourceTest,testsuite.regression.DataSourceRegressionTest*
 * https://www.oracle.com/java/technologies/java-archive-downloads-java-plat-downloads.html#7110-jndi-1.2.1-oth-JPR
 * click fscontext-1_2-beta3.zip, accept license to set download cookie, ignore sign-on form
 * https://download.oracle.com/otn-pub/java/jndi/1.2.1/fscontext-1_2-beta3.zip
 * or: *curl -L -b oraclelicense=140 http://download.oracle.com/otn-pub/java/jndi/1.2.1/fscontext-1_2-beta3.zip > fscontext.zip*
 * *unzip fscontext-1_2-beta3.zip lib/fscontext.jar lib/providerutil.jar*

Prepare environment:
::

 quilt push -a
 JAVA_HOME=/usr/lib/jvm/default-java

Run the testsuite:
::

 ant \
   -Dcom.mysql.jdbc.jdk5.javac=$JAVA_HOME/bin/javac \
   -Dcom.mysql.jdbc.jdk8.javac=$JAVA_HOME/bin/javac \
   -Dsnapshot.version= \
   -Dcom.mysql.jdbc.extra.libs=lib \
   -Dant.java.version=1.5 \
   -Dant.build.javac.source=1.6 \
   -Dant.build.javac.target=1.6 \
   \
   -Dcom.mysql.jdbc.testsuite.url='jdbc:mysql://localhost/testDB?user=root&password=root&serverTimezone=UTC' \
   -Dcom.mysql.jdbc.testsuite.jvm=$JAVA_HOME \
   -Dcom.mysql.jdbc3.testsuite.jvm.java=$JAVA_HOME/bin/java \
   -Dcom.mysql.jdbc4.testsuite.jvm.java=$JAVA_HOME/bin/java \
   -Dcom.mysql.jdbc42.testsuite.jvm.java=$JAVA_HOME/bin/java \
   -Dcom.mysql.jdbc.noCleanBetweenCompiles=true \
   compile-testsuite test

To select specific tests:
::
 
 -Dtest=testsuite.simple.StringUtilsTest
 -Dtest=testsuite.simple.ConnectionTest -Dmethods=testCacheDefaultTimezone 


The test suite should run in ~15mn.

There are some errors, make sure there are no more than in the previous upload. 

See also: https://dev.mysql.com/doc/connector-j/5.1/en/connector-j-testing.html


| Copyright (C) 2020  Sylvain Beucler
