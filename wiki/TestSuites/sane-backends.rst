=============
sane-backends
=============

-----
DEP-8
-----

The package ships `DEP-8 <https://dep-team.pages.debian.net/deps/dep8/>`_ smoketesting, see autopkgtest <https://wiki.debian.org/LTS/TestSuites/autopkgtest>`_ for the basic LXC setup.

In your test VM:
::

 # Test the package you're working on
 autopkgtest -B ../sane-backends_1.0.25-4.1+deb9u1_amd64.changes -- lxc autopkgtest-stretch-amd64
 # Test the package in the archive (bug in this autopkgtest version / lxc virt-server: should be the same as above)
 autopkgtest -B -- lxc autopkgtest-stretch-amd64


Note: currently the test appears to be broken, cf. DebianBug:968369.
::

 sed -e /:net/:pnm/ debian/tests/start-net


-------------------
Internal test-suite
-------------------

Normally run through *dh_auto_test*
::

 make check

------
Manual
------

Plug a physical network or USB scanner to your VM, and try scanning something.

`simple-scan <https://packages.debian.org/search?keywords=simple-scan>`_ or `xsane <https://packages.debian.org/search?keywords=xsane>`_ may help.


| Copyright (C) 2020, 2021  Sylvain Beucler
