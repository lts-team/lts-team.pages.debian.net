====
tiff
====

While exchanging with the package maintainer about a Stretch update, he discussed the tests he performed before upload (reworded):

 * Setup a Stretch/amd64 VM
 * Installed the updated tiff packages
 * Installed a bank of 50k images ranging from 800x600 to 10k resolution (average ~1.5k+ pixels)
 * Converted those from tiff to jpg and then vice-versa using CLI
 * Checked for any possible warning from the libraries
 * Checked a few thousands of those images randomly and manually, starting the viewer from CLI to see any potential warning messages, and visual checking for issues


| Copyright (C) 2020  Sylvain Beucler
