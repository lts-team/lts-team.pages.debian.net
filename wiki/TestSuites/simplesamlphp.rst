=============
SimpleSAMLphp
=============

SimpleSAMLphp is an authentication and federation application that supports
several protocols (SAML 2.0, Sihbboleth 1.3, OpenID, ...).

------------------------------
Service and Identity Providers
------------------------------

The easiest way to test it is to set up two different hosts, one that serves
as Service Provider and the other as Identity Provider. The debian package
includes a "userpass example" that can be used to test. The tests described
here are based on the v1.19 upstream documentation, that can be found at:
https://simplesamlphp.org/docs/1.19/. In debian, all the required configuration
files can be found under `/etc/simplesamlphp`. The x.509 certificates are to be
placed on `/etc/ssl/certs/`, and the admin password (that can be used in the
web GUI) is found at `/var/lib/simplesamlphp/secrets.inc.php`.

After enabling the apache2 simplesamlphp module (`a2enconf simplesamlphp`), the
web GUIs can be accessed at http://host/simplesamlphp/.

On the service provider:

In `authsources.php`, uncomment the `default-sp` configuration.

Once you have configured the identity provider (see below), include its
metadata in `/etc/simplesamlphp/metadata/saml20-idp-remote.php`.
To avoid errors, copy+paste the **Identity Provider** metadata in SimpleSAMLphp
flat file format that can be found at:
http://simplesamlphp-idp.test/simplesamlphp/ / `Federation` "tab" / "SAML 2.0
IdP Metadata".


On the identity provider (simplesamlphp-idp.test):

In `authsources.php`, uncomment the `'example-userpass'` array. In
`config.php`, include:

.. code::

     'module.enable' => array(
          'exampleauth' => TRUE, // Setting to TRUE enables.
          'saml' => TRUE, // Setting to FALSE disables.
          'core' => NULL, // Unset or NULL uses default.
     ),

Follow the upstream documentation related to `metadata/saml20-idp-hosted.php`.

In `metadata/saml20-sp-remote.php`, copy+paste the **Service Provider**
metadata, that can be found in its "Federation" web GUI page.

Test the Authentication service using the web GUI on the service provider /
"Authentication" tab / "Test configured authentication sources". You should
find there the `default-sp`, that points to the identify provider configured
above. Verify that you can log in with Username: `student` and Password:
`studentpass`.


--------------------
XML to SimpleSAMLphp
--------------------

To (partially) test XML handling, it is useful to use the XML to SimpleSAMLphp
metada converter tool that can be found in the "Federation" tab.

----------
Test suite
----------

The simplesamlphp test suite is not run at build time. It requires phpunit 4.8,
which is older than the 7.5.6-1 version found in buster. One method to run the
test suite is relying on composer to install the older dependencies.
Unfortunately, composer is not packaged in debian, so it has to be installed
from the upstream source: https://getcomposer.org. Once the dependencies are
installed, the test suite can be run with:

.. code:: sh

   ./vendor/bin/phpunit -c tools/phpunit/phpunit.xml
