========
ActiveMQ
========

Starting ActiveMQ:
::

 ln -s ../instances-available/main /etc/activemq/instances-enabled/
 service activemq restart
 tail /var/lib/activemq/main/data/activemq.log

Debian's ActiveMQ has many disabled modules (see
``debian/libactivemq-java.README.Debian``) so tools like the web
console don't work.

ActiveMQ comes with different examples available at
``/usr/share/doc/activemq/examples/``. Some are easy to compile and
test, such as ``openwire/cpp`` (included in stretch, also works in
jessie; see its ``readme.md``). The openwire example requires manually
installing activemq-cpp, which itself needs ``apt-get install
libapr1-dev libcppunit-dev``.

The STOMP ruby examples work easily too (the version in jessie is
broken, use stretch's). Copy the ``transportConnectors`` block from
``examples/conf/activemq-stomp.xml`` in your main configuration to
enable STOMP, restart activemq and check the ruby ``README.txt``.

Running tests from the test suite: TODO (``mvn`` is quite tricky to
use in Debian/offline environment).

| Copyright (C) 2024  Santiago Ruano Rincón
| Copyright (C) 2025  Sylvain Beucler
