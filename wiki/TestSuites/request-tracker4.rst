================
request-tracker4
================

request-tracker4 has an extensive test suite. In order to manually test new security updates you have to do the following:

 * apt install request-tracker4 rt4-apache2
 * Follow debconf configuration steps and remember the root password.
 * Note: You may hit Debian bug #869641 in Stretch. Just remove libjson-xs-perl as a workaround.
 * Edit /etc/apache2/sites-available/000-default.conf and add the Include line at the end of the VirtualHost block.
 * Include /etc/request-tracker4/apache2-modperl2.conf
 * a2enmod perl 
 * systemctl restart apache2
 * Open Firefox and go to http://localhost/rt. request-tracker4 will warn you about a potential CSRF attack and redirect you to your domain name which was chosen via debconf.
 * Login: root
   password: xxxxx (created via debconf)
 * Create tickets and test functionality.


| Copyright (C) 2022  Markus Koschany
