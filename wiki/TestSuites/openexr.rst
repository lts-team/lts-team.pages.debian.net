=======
openexr
=======

----------
Test suite
----------

The test suite is run on build using *make check* with results in *IlmImfTest/test-suite.log*

To run a specific test:
::

 cd IlmImfTest/
 ./IlmImfTest testCompression

*IlmImfTest* is rebuilt when running the top-level *make* no need to rebuild it separately after modifying the code.

--------
oss-fuzz
--------

Latest vulnerabilities are commonly found through the oss-fuzz project, which involves new internal functions, not yet available in stretch.

However the test cases are valid OpenEXR images that can be downloaded and run through openexr's standard CLI utilities.

So you can usually reproduce the issues with an :doc:`asan/ubsan <../../howtos/lts-Development-Asan>`-enabled build, e.g.:

::

 exrmakepreview poc /dev/null
 exrmaketiled poc /dev/null

---------
Smoketest
---------

::

 cd openexr-x.x/
 find -name "*.exr" | while read f; do convert $f ../$(basename $f).png; done
 geeqie ../*.png


Note: when built with :doc:`AddressSanitizer <../../howtos/lts-Development-Asan>`, this test Aborts, rebuild openexr normally first.


| Copyright (C) 2022  Sylvain Beucler
