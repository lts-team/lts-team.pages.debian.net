=======
python2
=======

The test suite runs on *debuild*, though test failures are ignored
(since there are many). This is being worked on. Meanwhile, most
module test suites have been fixed, do run them before and after
working on the matching Python modules.

The package ships `DEP-8
<https://dep-team.pages.debian.net/deps/dep8/>`_ tests but those
merely re-run parts of the test suites. Moreover, our DEP-8 skips many
tests by unsetting several test suite "resources", including "network"
due to limited/blocked network access within CI environments. Again,
do run these manually if you modify a related module.

To run a single test file before building/installing a new version:
::

 PYTHONPATH=Lib build-shared/python Lib/test/test_xmlrpc.py

Some other distros maintain legacy python2.7 support, e.g. `Ubuntu
<https://launchpad.net/ubuntu/+source/python2.7>`_ and `OpenSUSE
<https://opensuse.pkgs.org/15.6/opensuse-sle/python-2.7.18-150000.68.1.x86_64.rpm.html>`_.

| Copyright (C) 2020, 2023, 2025  Sylvain Beucler
