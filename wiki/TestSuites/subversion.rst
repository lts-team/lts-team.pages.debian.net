==========
subversion
==========

---------------------------
Reducing build/test latency
---------------------------

*DEB_BUILD_OPTIONS="parallel=6"* enables parallel build.

*PARALLEL=6* enables parallel running of tests in the test suite.

*DEB_BUILD_OPTIONS="no-javahl no-ruby"* disables building parts that might not be relevant.

To summarize: 
::

  export DEB_BUILD_OPTIONS="parallel=6 no-javahl no-ruby"
  export PARALLEL=6   # For running tests in parallel


To isolate specific tests:
::

  # Run a C test
  make -C BUILD/ check PARALLEL=6 TESTS=subversion/tests/libsvn_subr/dirent_uri-test
  # Run a Python test
  make -C BUILD/ check PARALLEL=4 TESTS=subversion/tests/cmdline/authz_tests.py

-----------------
svnserveautocheck
-----------------


Some tests require *svnserve* to be available, and are skipped by default. There is a separate make target *svnserveautocheck*, working the same as *check*, that does the setup automatically to run the test suite.

------------
Test results
------------

Test restuls are stored in *BUILD/tests.log*.

Note that if one looks at *tests.log* in a successful build, there are about 45 tests marked as XFAIL. If you feel like a patch broke a lot of tests, compare the results against a clean build of the unpached package.

------------------------
Test suite documentation
------------------------

Further documentation of the test suite can be found in *subversion/tests/cmdline/README*


| Copyright (C) 2022  Enrico Zini
