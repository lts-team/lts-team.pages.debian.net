=======
sqlite3
=======

The packages comes with an extensive testsuite that is not run automatically on build.

::

 debuild
 make test  # as non-root


Variants exist depending on the version:
::

 grep -B2 -A1 test: Makefile
 make fulltest
 make smoketest
 ...


To run a single set of tests:
::

 ./testfixture test/fts3corrupt4.test 


Package maintainer `mentions <https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=943509#87>`_: ''the upstream testing tools and cases are not open source.''


| Copyright (C) 2019  Sylvain Beucler
