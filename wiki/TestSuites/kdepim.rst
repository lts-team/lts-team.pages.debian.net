======
kdepim
======

------
jessie
------

~~~~~
kmail
~~~~~

Test incremental changes in e.g. templateparser/:
::

 debuild
 cd debian/tmp/
 # $EDITOR ...
 make -j $(nproc)
 make install DESTDIR=$(pwd)/destdir
 \cp -a destdir/usr/lib/libtemplateparser.so* /usr/lib/


For CVE-2019-10732, manually run recent test cases with encrypted e-mails:
::

 git clone git://anongit.kde.org/messagelib.git
 GNUPGHOME=$(pwd)/messagelib/messagecore/autotests/gnupg_home kmail --view file://$(pwd)/messagelib/templateparser/autotests/data/404698-gpg.mbox


| Copyright (C) 2019  Sylvain Beucler
