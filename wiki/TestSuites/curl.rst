====
curl
====

----------
Test suite
----------

The test suite is run on build, but multiple tests fail (in both
jessie and stretch) and this doesn't interrupt the build, so don't
trust it (this is fixed in bookworm).

Run the build as non-root otherwise some SSH-based tests are disabled.

You'll want to compare with a first rebuild, with matching environment, e.g. for deb9u13:
::

 $ grep -P '^TESTDONE|TESTFAIL' curl_xxx.build
 TESTDONE: 916 tests out of 922 reported OK: 99%
 TESTFAIL: These test cases failed: 165 1034 1035 1140 2046 2047 
 
 TESTDONE: 915 tests out of 921 reported OK: 99%
 TESTFAIL: These test cases failed: 165 1034 1035 1140 2046 2047 
 
 TESTDONE: 903 tests out of 920 reported OK: 98%
 TESTFAIL: These test cases failed: 165 310 311 312 313 571 1034 1035 1140 2034 2035 2037 2038 2041 2042 2046 2047 


The build copies the source 3 times to produce 3 build variants, making it harder to test a patch.

To run a specific test, e.g. test2081:
::
 
 debuild
 cd debian/buildXXX/tests/
 make
 srcdir=. /usr/bin/perl -I. ./runtests.pl -n 2081


| Copyright (C) 2021, 2024  Sylvain Beucler
