=====
Cacti
=====

Initial install:
::

  # Make sure the stack is installed and running first
  apt install apache2 libapache2-mod-php mariadb-server postfix
  a2dismod mpm_event
  a2enmod mpm_prefork
  service apache2 restart
  # Install and auto-configure Cacti
  apt install cacti

Visit http://localhost/cacti/ and login (admin:cacti).

Cacti on bullseye appears to suffer from installation issues due to
forcing going through install.php (or, I did something wrong during
the package installation). You may also need to complete this for
exploits that make use of the installation pages:

 * In case of infinite redirects, go to /cacti/auth_changepassword.php
   and change the initial password (admin:admin), respecting the
   password constraints (e.g. Admin123!)
 * MySQL TimeZone can be populated through ``mysql_tzinfo_to_sql /usr/share/zoneinfo/|mysql mysql``
 * Cacti-Install insists on setting ``date.timezone``, e.g. ``date.timezone = "Europe/Paris"`` in ``/etc/php/x.x/{apache2,cli}/php.ini``
 * Cacti-Install insists on getting write access to ``/usr/share/cacti/site/{resource,site}/``, you can ``chown -R root:root`` back after the install
 * Fix cron errors: ``chown www-data: /var/log/cacti/poller-error.log``

To create a simple SNMP-monitorable devices on localhost:
::

  apt install snmpd
  
  # optional: to use SNMP human-readable identifiers,
  # we need MIBs under the RFC (sadly non-free) license:
  apt install snmp-mibs-downloader  # + see README.Debian
  
  # to easily unlock more local graphs, strip down rights management:
  echo 'rocommunity public' > /etc/snmp/snmpd.conf  # one-liner conf
  service snmpd restart
  
  snmpget -v2c -c public localhost .1.3.6.1.2.1.1.3.0  # sysUpTime.0

You can then add a new Net-SNMP device for 127.0.0.1 in the web interface.

Cacti has a LOT of features, the more advanced of which aren't properly
handled in the web interface (such as additional pollers requiring
manually inserting entries in the database). I'd suggest making
targetted testing for each feature you patched.

Plugins: Cacti supports external plugins. There's a `list of current
plugins <https://forums.cacti.net/viewtopic.php?t=14985>`_. You can
try to install the simple `Thold
<https://github.com/Cacti/plugin_thold>`_ one.


| Copyright (C) 2025  Sylvain Beucler
