====
salt
====

----------
Test Suite
----------

The upstream test suite is available in the source directory.

Salt requires a `custom testing framework <https://github.com/saltstack/salt-testing>`_ prior `commit <https://gitlab.com/saltstack/open/salt/-/commit/3beb3fb80194b9576f7a353298fedfcb35e9f781|v2017.x>`_.

To kickstart the tests, you might need:

::

 apt build-dep salt
 apt install python-pip python-psutil python-crypto python-pytest-tornado python-yaml git python-git python-concurrent.futures python-mock python-zmq
 cd salt-2016.11.2+ds/ && debi  # for pulling additional run-time dependencies
 
 pip install git+https://github.com/saltstack/salt-testing.git#egg=SaltTesting
 # jessie: pip install https://github.com/saltstack/salt-testing/archive/refs/tags/v0.5.4.zip
 
 cd salt-2016.11.2+ds/ && python setup.py test


Behavior:
 * stretch: full test suite takes a good 5mn to start, runs for ~2h with many errors before running out of memory (VM with 2GB RAM + 2GB swap)
 * jessie: full test suite enters a timeouts loop

---------------
Unit tests only
---------------

::

 # All tests
 tests/runtests.py --unit-tests
 # Single test
 tests/runtests.py -n unit.version_test

--------
See also
--------

*salt-2016.11.2+ds/doc/topics/tests/index.rst* (`2015 online copy <https://ansible-cn.readthedocs.io/en/latest/topics/development/tests/>`_)

----------
Smoke test
----------

~~~~~~~~~~~~
Installation
~~~~~~~~~~~~

 * Install salt-master and salt-minion
   In *etc/salt/minion* change *master: salt* to *master: localhost* (or another test server) 
 * Restart services:
   ::

     master$ service salt-master start
     minion$ service salt-minion restart

 * Add minion key to master:
   ::

     master$ salt-key -L
     Accepted Keys:
     Unaccepted Keys:
     <your_minion_id>
     Rejected Keys:

     master$ salt-key -a 'your_minion_id'

~~~~~~~~~~~~~~
Basic commands
~~~~~~~~~~~~~~

::

  master$ salt 'your_minion_id' network.ip_addrs
  master$ salt 'your_minion_id' test.ping

~~~~~~~~~~~~~~~~
SSH-based access
~~~~~~~~~~~~~~~~

Don't install salt-minion.

Setup SSH keys:
:: 

  echo 'your_minion_id: 192.168.X.X' >> /etc/salt/roster
  salt-ssh your_minion_id -r 'hostname'  # generates master's SSH key if not already
  ssh-copy-id -i /etc/salt/pki/master/ssh/salt-ssh.rsa.pub root@192.168.X.X


Test:
::

  salt-ssh your_minion_id -r 'hostname'
  salt-ssh your_minion_id -r 'ip a'
  salt-ssh your_minion_id test.version
 

See also: https://docs.saltproject.io/en/latest/topics/ssh/

~~~~~~~~~~~~~~~~~~~
Simple Salt formula
~~~~~~~~~~~~~~~~~~~

Place following yaml file in *srv/salt* (by default *etc/salt/master/* declares *srv/salt/* as one "base"):

*EDITOR /srv/salt/webserver.sls*

::

  webserver:
    pkg:
      - installed
      - pkgs:
        - apache2
        - php
        - php-mysql


To test,
::

  master$ salt     'minion_id' state.sls webserver
  master$ salt-ssh 'minion_id' state.sls webserver

~~~~~~~~~~
TLS module
~~~~~~~~~~

On the minion:
::

  salt-call tls.create_ca my_little
  salt-call tls.create_csr my_little CN=second
  salt-call tls.create_self_signed_cert myself


See also: https://docs.saltproject.io/en/latest/ref/modules/all/salt.modules.tls.html


| Copyright (C) 2020  Abhijith PA
| Copyright (C) 2021  Markus Koschany
| Copyright (C) 2021  Sylvain Beucler
