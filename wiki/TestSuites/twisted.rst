=======
twisted
=======

---------------
Full test suite
---------------

**bullseye:** same as buster and stretch, but python2 support was dropped,
so only use `trial3`, not `trial`.


**buster:** same as stretch, but it checks for various libraries
Debian ships older versions of. Quick&dirty fix:
::

 rm src/Twisted.egg-info/requires.txt


**stretch:**

The package ships `DEP-8
<https://dep-team.pages.debian.net/deps/dep8/>`_ tests that run the
test suite with Python 2 and 3, see :doc:`autopkgtest`.

Some Unicode test fails with Python 2 when ran through autopkgtest in
a stretch VM (yet passes when run directly, and even on Salsa).


Manually / fine-grained:
::

 apt install tox
 cd twisted-16.6.0/
 apt build-dep .
 chown -R nonroot: .
 # cf. README.rst
 su nonroot -c 'tox -e py27-tests'
 su nonroot -c 'tox -e py35-tests'
 # TODO: add option/config to use system packages, considered missing
 
 apt install python-twisted-core python3-twisted  # for 'trial' and 'trial3'
 su nonroot -c 'PYTHONPATH=src/ trial twisted'  # python2
 su nonroot -c 'PYTHONPATH=src/ trial3 twisted' # python3
 su nonroot -c 'PYTHONPATH=src/ trial twisted.web'
 su nonroot -c 'PYTHONPATH=src/ trial twisted.web.test.test_http'
 su nonroot -c 'PYTHONPATH=src/ trial twisted.web.test.test_http.ParsingTests'
 
 # DEP-8 tests do a variant like this, not suitable for source testing:
 su nonroot -c 'python -m twisted.trial ./twisted/web/test/test_agent.py'
 su nonroot -c 'python3 -m twisted.trial ./twisted/web/test/test_agent.py'
 # Note: requires a twisted to be already installed system-wide
 # Note: if in src/ some issue with catching warnings occurs and some tests fail
 # Note: if not in src/ py3+twisted.trial will use the twisted from /usr/lib/

**jessie:**
::

 cd twisted-14.0.2/
 # cf. README
 chown -R nonroot: .
 su nonroot -c 'bin/trial twisted'
 su nonroot -c 'bin/trial twisted.web'
 su nonroot -c 'bin/trial twisted.web.test.test_http'
 su nonroot -c 'bin/trial twisted.web.test.test_http.ParsingTestCase'

There are dozens of errors in the test suite.  Among others, this
stalls/timeouts during SSL tests AFAICS.  Still it's useful to test
modules changes by the recent patches.


----------
Smoke test
----------

twisted-web:
::

 apt install apache2  # for a sample /var/www/html/

 twistd -n web --path /var/www/html/
 twistd3 -n web --path /var/www/html/
 curl http://127.0.0.1:8080/
 
 cp -a docs/web/examples/hello.rpy.py /var/www/html/hello.rpy
 # jessie: cp -a doc/_downloads/hello.rpy.py /var/www/html/hello.rpy
 # py3: fix: resource = static.Data(d.encode('utf-8'), ...
 # stretch: py3: TODO: didn't find how to enable .rpy execution
 curl http://127.0.0.1:8080/hello.rpy
 
 python docs/web/examples/getpage.py http://debian.org
 python3 docs/web/examples/getpage.py http://debian.org
 # jessie: python doc/_downloads/getpage.py http://debian.org
 # py3: fix: sys.argv[1].encode('ascii')
 
 python docs/web/howto/listings/client/cookies.py
 python3 docs/web/howto/listings/client/cookies.py
 # jessie: python doc/_downloads/cookies.py 
 # stretch: py3: use compat compat.cookielib.CookieJar and use b'...' in agent.request(...)


| Copyright (C) 2022, 2023, 2024  Sylvain Beucler
