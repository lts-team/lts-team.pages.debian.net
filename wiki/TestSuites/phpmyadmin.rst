==========
phpmyadmin
==========

------
Jessie
------

Lots of errors/incomplete/skipped even with the upstream source, lots of deprecation warnings.
The unit tests quickly halts on Debian's patched codebase due to removing bundled libraries and getFilePath()/CVE-2016-6621.

The Selenium tests can be run from the upstream phpmyadmin source while targetting a Debian install.
The testsuite recommends compiling and installing PECL runkit for additional tests, but it makes it crash/halt.
It is not entirely stable, here are 2 full runs on +deb8u4:
::

 Tests: 2192, Assertions: 4800, Failures: 4, Errors: 120, Incomplete: 9, Skipped: 93.
 Tests: 2192, Assertions: 4798, Failures: 4, Errors: 122, Incomplete: 8, Skipped: 93.

+deb8u5:
::

 Tests: 2192, Assertions: 4802, Failures: 4, Errors: 120, Incomplete: 9, Skipped: 91.
 Tests: 2192, Assertions: 4802, Failures: 4, Errors: 120, Incomplete: 9, Skipped: 91.

(most of the Errors are actually {{{PHPUnit_Framework_Assert::assertTag is deprecated}}})

That's still an indicator on whether an update significantly broke something :)

Install instructions:

::

 apt install phpunit-selenium ant php5-gd php5-gmp
 mkdir -p /usr/share/selenium/
 # using the latest selenium 2.x (didn't try 3.x)
 wget -c http://selenium-release.storage.googleapis.com/2.53/selenium-server-standalone-2.53.1.jar \
      -O /usr/share/selenium/selenium-server.jar
 
 # Needs old Firefox 58 (not 60) otherwise Selenium can't install its extension
 wget http://ftp.fr.debian.org/debian/pool/main/f/firefox-esr/firefox-esr_52.8.1esr-1~deb8u1_amd64.deb
 wget http://ftp.fr.debian.org/debian/pool/main/f/firefox-esr/firefox-esr-l10n-fr_52.8.1esr-1~deb8u1_all.deb
 cat <<'EOF' > firefox.SUMS
 201f76fa345cd54549277e8a851eb8e240128100f43cd59161822154a12ddb7d  firefox-esr_52.8.1esr-1~deb8u1_amd64.deb
 1557cdbabd350380cf864fd0c8b21f51bcb77f9b738a22c62ff5a1a0c27d70c5  firefox-esr-l10n-fr_52.8.1esr-1~deb8u1_all.deb
 EOF
 sha256sum -c firefox.SUMS
 apt install libjsoncpp0
 dpkg -i *.deb
 
 # In a graphical session (possibly disable the screen saver):
 java -jar /usr/share/selenium/selenium-server.jar

::

 # additional selenium tests (headers) require runkit
 # well drop that - actually that make the non-selenium testsuite crash...
 wget http://pecl.php.net/get/runkit-1.0.4.tgz
 apt install php5-dev
 tar xzf runkit-1.0.4.tgz
 cd runkit-1.0.4/
 # README
 phpize
 ./configure
 make
 make test
 make install
 cat <<'EOF' > /etc/php5/mods-available/runkit.ini
 extension=runkit.so
 runkit.internal_override=1
 EOF
 #php5enmod runkit
 
::

 git clone https://github.com/phpmyadmin/phpmyadmin/
 cd phpmyadmin/
 git checkout RELEASE_4_2_12
 # Note: build.xml => phpunit --configuration phpunit.xml.dist => test/bootstrap-dist.php
 edit test/bootstrap-dist.php:
 #    'TESTSUITE_PASSWORD' => 'mysql_root_password',
 #    'TESTSUITE_SELENIUM_HOST' => '127.0.0.1',
 
 ant


You should see browser windows popping in and out.
Takes ~10mn per run.

Original mail: https://lists.debian.org/debian-lts/2019/02/msg00170.html


| Copyright (C) 2019, 2020  Sylvain Beucler
