===
php
===

------------------------
Other long-term branches
------------------------

When checking your triage and backports, you can compare your work to other non-official branches:

 * sury:

  * https://packages.sury.org/php/pool/main/p/php7.4/
  * https://packages.sury.org/php/pool/main/p/php7.3/
  * https://packages.sury.org/php/pool/main/p/php7.0/
  * https://packages.sury.org/php/pool/main/p/php5.6/

 * shivammathur:

  * https://github.com/shivammathur/php-src-backports/tree/PHP-7.4-security-backports
  * https://github.com/shivammathur/php-src-backports/tree/PHP-7.3-security-backports
  * https://github.com/shivammathur/php-src-backports/tree/PHP-7.0-security-backports
  * https://github.com/shivammathur/php-src-backports/tree/PHP-5.6-security-backports
  * https://github.com/shivammathur/php-src-backports/tree/PHP-5.6-security-backports-openssl11

 * discontinued:

  * https://github.com/microsoft/php-src/tree/PHP-7.0-security-backports
  * https://github.com/microsoft/php-src/tree/PHP-5.6-security-backports

------------------
Default test suite
------------------

The test suite is run on build, unfortunately it has many errors and its failure doesn't stop the build.

Interestingly TEST_PHP_CGI_EXECUTABLE has been set to an incorrect path for years `#991008 <https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=991008>`_, resulting in many additional errors.

It is recommended to compare ``test-results.txt`` after stripping a few build-specific details:
::

 sed -e 's|TEST [0-9]\+/[0-9]\+|TEST x/x|' -e "s|$(pwd)|/<<PKGBUILDDIR>>|g" test-results.txt > test-results.txt2
 diff -u .../previous-rebuild/test-results.txt2 test-results.txt2 | less


-----------------
Run a single test
-----------------

(after a first ``debuild``)

::

 # quilt pop/push...
 make -C cli-build/ && make -C ext-build/
 cli-build/sapi/cli/php ./run-tests.php -P -n ext/filter/tests/
 cli-build/sapi/cli/php ./run-tests.php -P -n -d extension_dir=ext-build/modules/ -d extension=soap.so ext/soap/tests/bug80672.phpt


More environment variables / options may need to be set, see ``debian/rules:test-results.txt``.

---
FPM
---

The test suite does not run by default due to the separate builds directory in the Debian build.
::

 su - non-root-user
 fpm-build/sapi/cli/php ./run-tests.php -P -n sapi/fpm/tests/

------
jessie
------

::

 make -C cli-build/ && make -C apache2-build/
 cli-build/sapi/cli/php ./run-tests.php -P -n \
   -d extension_dir=apache2-build/modules/ -d extension=pdo.so -d extension=interbase.so -d extension=pdo_firebird.so \
   ext/pdo_firebird/tests/bug_47415.phpt


| Copyright (C) 2021, 2023, 2024  Sylvain Beucler
