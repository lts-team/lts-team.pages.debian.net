====
QEMU
====

QEMU is a versatile piece of software that has lots of different use cases. Here are some common configurations to (manually...) check.

Emulation mode:
 * throw-away, user-mode/non-accelerated emulation -- `here are some images <https://people.debian.org/~gio/dqib/>`_
 * KVM guest (incompatible with Xen)
 * Xen HVM guest

User interfaces:
 * virt-manager/libvirt
 * gnome-boxes
 * ProxMox is Debian-based and would be a good test case, but they ship their own modified qemu version
 * Xen *xl* CLI

Guest systems:
 * GNU/Linux
 * Windows: trial versions of Windows Server ISOs are easily available; pre-installed VMs are also available through modern.ie,
   with a `conversion script <https://github.com/lentinj/ie-vm>`_; 2012R2 is known to crash with Xen HVM on first boot (Jessie)

Networking card:
 * basic Ethernet emulation
 * VirtIO networking (accelerated)

Networking attachment:
 * slirp user-mode networking (NAT)
 * bridge
 * ...

Disk types:
 * IDE, SCSI emulation (non-accelerated)
 * VirtIO disk (accelerated, may require `drivers ISO <https://www.linux-kvm.org/page/WindowsGuestDrivers/Download_Drivers>`_)
 * iSCSI client/initiator (you can install `tgt <https://packages.debian.org/tgt>`_ on a separate box for a test server/target, and *qemu-block-extra*
 * `VirtIO crypto <https://wiki.qemu.org/Features/VirtioCrypto#Testing>`_

Graphic access:
 * SDL (direct window)
 * VNC (e.g. with vncviewer)
 * Spice (e.g. with virt-manager)

-------------
Common issues
-------------

Make sure you reserve enough memory (``-m``). QEMU's default is 128M
but running the Debian Installer ISO plain panics with that amount
(512M only gets you buster's "low-memory" mode).

----------
ASAN build
----------

:doc:`ASan <../../howtos/lts-Development-Asan>` doesn't support static builds, so in addition to the *DEB_*FLAGS_APPEND* drop qemu-user/qemu-user-static from *debian/control* and *debian/control-in* before building.


| Copyright (C) 2019, 2020, 2021, 2022, 2023  Sylvain Beucler
