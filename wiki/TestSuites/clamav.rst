======
clamav
======

ClamAV requires up-to-date version to process the latest virus
information from their online database:

 * https://docs.clamav.net/faq/faq-eol.html
 * https://blog.clamav.net/2023/05/end-of-life-eol-policy-change-0103-one.html

so we usually backport new releases, after a Debian SUA to avoid
breaking dist upgrades:

 * https://lists.debian.org/debian-stable-announce/2023/09/msg00000.html
 * https://lists.debian.org/debian-lts/2018/03/msg00033.html
 * https://lists.debian.org/debian-lts/2019/03/msg00161.html

ClamAV also depends on Rust, which may require a backport of its own:

 * https://docs.clamav.net/faq/faq-rust.html

Jessie test instructions:
 * `<https://lists.debian.org/debian-lts/2019/04/msg00117.html>`_

| Copyright (C) 2024  Sylvain Beucler
