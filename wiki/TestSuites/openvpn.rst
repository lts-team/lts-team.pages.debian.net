=======
OpenVPN
=======

OpenVPN comes with basic, single-machine configuration DEP8 tests,
which require ``isolation-machine`` and are currently *not* run on
Salsa-CI.  See :doc:`autopkgtest` for how to run them in a full VM
environment.

In addition, do make a full 2-machines VPN test, outside of the
constrained/simulated/single-machine autopkgtest environments. This
tests separate network stacks as well as the systemd service
units. You can clone 2 bullseye-lts VMs for this.

A simple client/server configuration is provided at:
https://openvpn.net/community-resources/static-key-mini-howto/

In the future (>= v2.6) the static key method is being deprecated and
replaced by ``peer-fingerprint``:
https://github.com/openvpn/openvpn/blob/master/doc/man-sections/example-fingerprint.rst

OpenVPN provides systemd instantiated service units:

.. code-block:: bash

  # VM1
  systemctl enable openvpn-server@server  # /etc/openvpn/server/server.conf
  systemctl restart openvpn-server@server

  # VM2
  systemctl enable openvpn-client@alice  # /etc/openvpn/client/alice.conf
  systemctl restart openvpn-client@alice

| Copyright (C) 2025  Sylvain Beucler
