======
galera
======

Tutorial to setup a minimal galera-4 cluster and test replication:
  https://www.digitalocean.com/community/tutorials/how-to-configure-a-galera-cluster-with-mariadb-on-debian-10-servers

Use Galera and MariaDB straight from bullseye, and drop ``-p`` since
MariaDB authentifies root access through Unix socket by default.

Official documentation links are behind a contact form, but this seems to be current:
  https://galeracluster.com/library/training/tutorials/getting-started.html

| Copyright (C) 2024  Sylvain Beucler
