=====
netty
=====

Casual test: there are examples in *src/java/main/org/jboss/netty/example* (3.x) or *example/src/main/java/io/netty/example* (later).

Jessie example:
::

 cd netty-3.2.6.Final/src/main/java/
 javac -cp /usr/share/java/netty-3.2.6.Final.jar org/jboss/netty/example/http/file/*.java
 java -cp /usr/share/java/netty-3.2.6.Final.jar:. org.jboss.netty.example.http.file.HttpStaticFileServer
 curl http://127.0.0.1:8080/


::

 cd netty-3.9-3.9.0.Final/src/main/java/
 javac -cp /usr/share/java/netty-3.9.0.Final.jar org/jboss/netty/example/http/upload/HttpUpload*.java org/jboss/netty/example/securechat/SecureChat*.java org/jboss/netty/example/telnet/Telnet*.java
 java -cp /usr/share/java/netty-3.9.0.Final.jar:. org.jboss.netty.example.http.upload.HttpUploadServer


Test suite runs automatically on build (3.2).

To debug headers with 3.2, edit the example above with e.g.:
::

 import java.util.Map.Entry;
 ...
     public void messageReceived(ChannelHandlerContext ctx, MessageEvent e) throws Exception {
         HttpRequest request = (HttpRequest) e.getMessage();
 
         for (Entry<String, String> entry: request.getHeaders()) {
             System.out.print("HEADER: " + entry.getKey() + '=' +
                                    entry.getValue() + "\r\n");
         }
         System.out.print("\r\n\r\n");


| Copyright (C) 2020  Sylvain Beucler
