=======================
OpenStack in buster LTS
=======================

 * https://lists.debian.org/debian-lts/2022/08/msg00005.html : list of packages, buster status

 * https://lists.debian.org/debian-lts/2022/08/msg00014.html : zigo's offer for manual regression testing before we issue a DLA


| Copyright (C) 2022  Sylvain Beucler
