======
NodeJS
======

Running the JS test suite:
::

  ./debian/rules check  # make test-ci-js

Caution: some tests fail when run as root.

Running a single JS test:
::

  debuild
  
  OPENSSL_CONF=./deps/openssl/openssl/apps/openssl.cnf \
    ./node test/parallel/test-tls-passphrase.js

Native tests:
::

  apt install node-gyp
  touch test/addons/.docbuildstamp
  apt install npm
  npm install graceful-fs  # apt install node-graceful-fs doesn't work; ignore errors..
  make test-ci-native
  # 'addons/dlopen-ping-pong/test' appears broken, other tests OK

Other native tests (e.g. if a CVE fix changes a test in ``test/cctest``) (needs to rebuild lots):
::

  make cctest

Rebuild reverse-dependencies (to validate some 3rd-party code), e.g.:
::

 apt source node-thenify
 cd node-thenify-*/
 apt build-dep .
 debuild
