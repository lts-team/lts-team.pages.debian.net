=======
emacs24
=======

Manual tests
============

There are some upstream tests in ``test/manual/`` that have to be run
manually.  Of course, they expect Emacs to be built, but ``debian/rules`` does
out-of-tree builds to handle the separate binary packages.  Doing an in-tree
build is not completely straightforward because of the DFSG split: upstream's
build system expects to find doc/Makefile.in, but that's in a different source
package.

Here is a recipe to do an upstream, in-tree build on stretch.  You'll probably
need to replace ``/home/spwhitton/src/lts/emacs``.  I obtained this by doing
``debian/rules build`` and SIGINT'ing it once got as far as compiling C code.
Then I extracted these commands from the output.::

  quilt push -a
  ./autogen.sh
  cp -a /usr/share/misc/config.guess .
  cp -a /usr/share/misc/config.sub .
  CFLAGS="-g -O2 -fdebug-prefix-map=/home/spwhitton/src/lts/emacs=. -fstack-protector-strong -Wformat -Werror=format-security -Wall -fno-PIE" CPPFLAGS="-Wdate-time -D_FORTIFY_SOURCE=2" LDFLAGS="-Wl,-z,relro -no-pie" REL_ALLOC=no ./configure --build x86_64-linux-gnu --prefix=/usr --sharedstatedir=/var/lib --libexecdir=/usr/lib --localstatedir=/var/lib --infodir=/usr/share/info --mandir=/usr/share/man --with-pop=yes --enable-locallisppath=/etc/emacs24:/etc/emacs:/usr/local/share/emacs/24.5/site-lisp:/usr/local/share/emacs/site-lisp:/usr/share/emacs/24.5/site-lisp:/usr/share/emacs/site-lisp --without-gnutls --build x86_64-linux-gnu --prefix=/usr --sharedstatedir=/var/lib --libexecdir=/usr/lib --localstatedir=/var/lib --infodir=/usr/share/info --mandir=/usr/share/man --with-pop=yes --enable-locallisppath=/etc/emacs24:/etc/emacs:/usr/local/share/emacs/24.5/site-lisp:/usr/local/share/emacs/site-lisp:/usr/share/emacs/24.5/site-lisp:/usr/share/emacs/site-lisp --without-gnutls  --with-x=no --without-gconf --without-gsettings
  make -j1

jessie is the same but with this configure command:::

  CFLAGS="-g -O2 -fstack-protector-strong -Wformat -Werror=format-security -Wall" CPPFLAGS="-D_FORTIFY_SOURCE=2" LDFLAGS="-Wl,-z,relro" ./configure --build x86_64-linux-gnu --prefix=/usr --sharedstatedir=/var/lib --libexecdir=/usr/lib --localstatedir=/var/lib --infodir=/usr/share/info --mandir=/usr/share/man --with-pop=yes --enable-locallisppath=/etc/emacs24:/etc/emacs:/usr/local/share/emacs/24.4/site-lisp:/usr/local/share/emacs/site-lisp:/usr/share/emacs/24.4/site-lisp:/usr/share/emacs/site-lisp --build x86_64-linux-gnu --prefix=/usr --sharedstatedir=/var/lib --libexecdir=/usr/lib --localstatedir=/var/lib --infodir=/usr/share/info --mandir=/usr/share/man --with-pop=yes --enable-locallisppath=/etc/emacs24:/etc/emacs:/usr/local/share/emacs/24.4/site-lisp:/usr/local/share/emacs/site-lisp:/usr/share/emacs/24.4/site-lisp:/usr/share/emacs/site-lisp  --with-x=no --without-gconf --without-gsettings

| Copyright (C) 2025  Sean Whitton
