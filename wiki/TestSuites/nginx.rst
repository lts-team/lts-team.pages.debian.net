=====
nginx
=====

-----
DEP-8
-----

The package ships `DEP-8 <https://dep-team.pages.debian.net/deps/dep8/>`_ smoketesting.

In your test VM:
::

 cd nginx-*/
 debuild
 apt install autopkgtest
 
 adt-run -B ./ --- null
 adt-run --testname light-module-deps -B ./ --- null
 adt-run ../nginx_*_amd64.changes --- null


adt-run is easily confused by e.g. nginx-light conflicting with nginx-full, so you need to setup a local APT archive:
::
 
 cd /usr/src/nginx/
 apt-ftparchive packages . > Packages
 apt-ftparchive release . > Release
 echo 'deb [trusted=yes] file:///usr/src/nginx ./' >> /etc/apt/sources.list
 apt update

TODO: check https://github.com/nginx/nginx-tests/


-------
ngx_lua
-------

ngx_lua is bundled in ``debian/modules/``
(up to bullseye included, then in its own libnginx-mod-http-lua package):
::

 apt install nginx-extras
 service nginx restart
 
 # Test::Nginx
 apt install libtext-diff-perl libtest-longstring-perl libtest-lwp-useragent-perl libwww-perl libtest-base-perl
 git clone https://github.com/openresty/test-nginx
 cd test-nginx/
 perl Makefile.PL 
 make
 
 ## Recreate nginx-tap from openresty
 ## TODO: apparently this is specific to openresty/nginx-dtrace anyway
 #apt install systemtap linux-headers-amd64
 #cat <<'EOF' > /usr/local/bin/stap-nginx
 ##!/bin/bash
 #stap -d /usr/sbin/nginx "$@"
 #EOF
 #chmod 755 /usr/local/bin/stap-nginx
 
 # Install ngx_memc
 git clone https://github.com/openresty/memc-nginx-module debian/modules/memc-nginx-module
 # debian:rules: add to extras_configure_flags: --add-dynamic-module=$(MODULESDIR)/memc-nginx-module
 debuild
 cp -a ./debian/build-extras/objs/ngx_http_memc_module.so /usr/lib/nginx/modules/
 apt install memcached
 # remember to remove the module from the packaging...
 
 export TEST_NGINX_LOAD_MODULES=$(echo /usr/lib/nginx/modules/*)
 #export TEST_NGINX_USE_STAP=1
 prove -I /usr/src/test-nginx/lib -r t
 prove -I /usr/src/test-nginx/lib t/020-subrequest.t 

 
See also:
 * https://github.com/openresty/lua-nginx-module/#test-suite
 * https://github.com/openresty/lua-nginx-module/blob/master/util/build.sh


| Copyright (C) 2020, 2025  Sylvain Beucler
| Copyright (C) 2020  Salvatore Bonaccorso
