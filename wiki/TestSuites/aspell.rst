======
aspell
======

See https://github.com/GNUAspell/aspell-fuzz , which is the target used by the OSS-Fuzz project.

Is it reasonably easy to recompile it against Debian's libaspell and you can test the oss-fuzz reproducer.


| Copyright (C) 2019  Sylvain Beucler
