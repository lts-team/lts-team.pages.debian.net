======
zabbix
======

-------------
Client/server
-------------

::

 apt install apache2 php-mysql zabbix-frontend-php mysql-server zabbix-server-mysql


Follow README.Debian file\ *s* (zabbix-server-mysql, zabbix-frontend-php) or `<https://wiki.debian.org/Zabbix#Installation>`_.

http://192.168.x.x/zabbix/  # Admin : zabbix

Check Configuration > Hosts > Localhost > Enable, then Monitoring > Screens > Zabbix server.

On a separate bare system:

::

  apt install zabbix-agent


Edit /etc/zabbix/zabbix_agentd.conf, edit:
::

 Server=192.168.x.x
 ...
 ServerActive=192.168.x.x


Check /var/log/zabbix-agent/zabbix_agentd.log

In the web interface, Configuration > Host > Create Host, add a new host, give it a Template Linux OS.

In Monitoring > Screens, create a new screen and add a few graphs to check if the communication with the agent is OK.

----------
Test suite
----------

::

 apt install phpunit php-bcmath php-mbstring

 # Tests removed from source archive (in 'make dist'), add them back:
 git clone https://git.zabbix.com/scm/zbx/zabbix.git zabbix-git
 (cd zabbix-git && git checkout 3.0.32)
 cd .../zabbix-3.0.32+dfsg/
 cp -a ../zabbix-git/frontends/php/tests frontends/php/
 cp -a ../zabbix-git/build.xml .
 
 ant test-phpunit


3 errors with 3.0.31 (apparently spurious).

The front-end testsuite is more complex to setup.

TODO: incomplete

::

 apt install composer php-curl
 composer require phpunit/phpunit-selenium=3.0.3
 
 WORKSPACE_URL=http://192.168.x.x/zabbix/ ant -DDBTYPE=mysql -DDBHOST=localhost -DDBNAME=zabbix -DDBUSER=zabbix -DDBPASSWORD=SECRETPASSWORD create-configs
 
 ant test-frontend


Not registered in ant?

::

 phpunit --bootstrap bootstrap.php agent.php 


| Copyright (C) 2020, 2021, 2022  Sylvain Beucler
