=======
python3
=======

The test suite should run on *debuild*

The package also ships `DEP-8
<https://dep-team.pages.debian.net/deps/dep8/>`_ tests, which mostly
re-run the testsuite.

The network tests often rely on local certificatest that may have
expires, sync them from upstream if necessary.

To run a single test file before building/installing a new version:
::

 PYTHONPATH=Lib python3 Lib/test/test_xmlrpc.py


| Copyright (C) 2020, 2024  Sylvain Beucler
