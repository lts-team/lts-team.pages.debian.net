=====
rails
=====

-----
DEP-8
-----

The package ships `DEP-8 <https://dep-team.pages.debian.net/deps/dep8/>`_ smoketesting, see `autopkgtest <https://wiki.debian.org/LTS/TestSuites/autopkgtest>`_.

In your test VM:
::

 # buster
 cd rails-5.2.2.1+dfsg/
 debuild
 autopkgtest ../rails_5.2.2.1+dfsg-1+deb10u5_amd64.changes -- null

 # jessie/stretch
 cd rails-4.1.8/
 apt-get build-dep rails
 debuild
 
 apt install autopkgtest
 apt install ruby-spring ruby-jquery-rails ruby-sqlite3 ruby-sass-rails ruby-uglifier ruby-coffee-rails ruby-turbolinks ruby-jbuilder ruby-sdoc
 apt install ruby-byebug ruby-web-console # stretch
 
 adt-run -B ./ --- null
 adt-run ../rails_4.1.8-1+deb8u7_amd64.changes --- null

----------------
Rails test suite
----------------

Run the source functional testsuite:
::

 apt install ruby-nokogiri ruby-rack-cache ruby-bcrypt ruby-redcarpet ruby-pg ruby-mysql2 sqlite3 memcached mysql-server postgresql
 apt install ruby-mysql  # jessie
 gem install mocha --version '~> 0.14'
 gem install redcarpet --version '~> 2.2.2'
 gem install minitest --version '5.1'
 gem install w3c_validators kindlerb dalli benchmark-ips
 # Setup MySQL / PostgreSQL
 mysql -e "CREATE USER 'rails';"
 mysql -e "GRANT ALL PRIVILEGES ON activerecord_unittest.* TO 'rails';"
 mysql -e "GRANT ALL PRIVILEGES ON activerecord_unittest2.* TO 'rails';"
 mysql -e "GRANT ALL PRIVILEGES ON inexistent_activerecord_unittest.* TO 'rails';"
 su - postgres -c 'createuser --superuser '$USER
 (cd activerecord && rake db:create)
 
 time rake smoke > smoke-out.txt 2> smoke-err.txt  # ~35mn
 grep 'failures,' smoke-out.txt

 time rake test > out.txt 2> err.txt  # ~12mn
 grep 'failures,' out.txt
 
 # clean-up
 (cd activerecord && rake db:drop)
 rm -f activerecord/test/fixtures/fixture_database.sqlite3 activerecord/test/fixtures/fixture_database_2.sqlite3 Gemfile.lock activerecord/debug.log activerecord/test/config.yml railties/log/development.log


Single test:
::

 ruby -I actionpack/lib:actionpack/test actionpack/test/controller/parameters/accessors_test.rb
 ruby -I activesupport/lib:activesupport/test activesupport/test/caching_test.rb -n test_raw_values_with_marshal


See also:
 * https://github.com/rails/rails/blob/master/guides/source/contributing_to_ruby_on_rails.md#running-tests
 * https://guides.rubyonrails.org/development_dependencies_install.html#database-configuration
 * https://github.com/rails/rails/blob/5-2-stable/ci/travis.rb

----------------------------------
Rebuild third-party rails packages
----------------------------------

Updates in rails may cause third-party modules to break (see e.g. 2:5.2.4.3+dfsg-2).
::

 apt source ruby-rails-assets-xxx
 apt build-dep ruby-rails-assets-xxx
 cd ruby-rails-assets-xxx-*/
 debuild
 
 apt source ruby-jquery-xxx-rails
 apt build-dep ruby-jquery-xxx-rails
 cd ruby-jquery-xxx-rails-*/
 debuild


| Copyright (C) 2020, 2021, 2023  Sylvain Beucler
