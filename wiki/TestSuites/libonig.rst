=======
libonig
=======


libonig is a regex library for multi-bytes strings; it's used in particular by php-mbstring (either as embedded copy or linked to Debian's libonig, depending on Debian dist).

Smoke testing:

::

 cd libonig-xxx/
 debuild
 make check


Sample direct C usage: usually for libonig 6.x, for libonig 5.x, drop *onig_initialize()* which doesn't exist yet and isn't needed.
 * https://github.com/kkos/oniguruma/issues/147
 * https://github.com/kkos/oniguruma/issues/164

PHP test suite:
::

 cd php5-xxx/
 debuild
 cd cli-build
 TEST_PHP_EXECUTABLE=sapi/cli/php sapi/cli/php ../run-tests.php ../ext/mbstring/


PHP script:
::

 <?php
 var_dump(mb_ereg("X+++", "hello"));


| Copyright (C) 2019  Sylvain Beucler
