========
libsepol
========

Running the test suite
----------------------

On at least stretch, buster and bullseye.

1.  apt-get install libcunit1-dev libncurses-dev libfl-dev
2.  apt-get build-dep libsepol checkpolicy
3.  cd .../libsepol
4.  sed -i '/override_dh_auto_test:/d' debian/rules
5.  cd ..
6.  rm -rf checkpolicy*
7.  apt-get source checkpolicy
8.  mv checkpolicy-* checkpolicy
9.  cd checkpolicy
10. DEB_HOST_MULTIARCH=x86_64-linux-gnu make
11. cd ../libsepol
12. dpkg-buildpackage -b

| Copyright (C) 2024  Sean Whitton
