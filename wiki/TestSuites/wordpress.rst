=========
wordpress
=========

----------
Test Suite
----------

The upstream test suite is available in the root repository:

::

 svn co http://develop.svn.wordpress.org/branches/4.7/
 # or
 git clone https://github.com/WordPress/wordpress-develop -b 4.7


Work-around Trac's faulty IDS (403) in tests/phpunit/includes/trac.php:
::

 public static function isTracTicketClosed( $trac_url, $ticket_id ) {
     stream_context_set_default(['http'=>['user_agent'=>'curl/7.52.1']]);


Configure the test database:

::

 apt install ant phpunit mariadb-server
 mysql
 > CREATE DATABASE wptest;
 > GRANT ALL PRIVILEGES ON wptest.* TO "wptest" IDENTIFIED BY 'wptest';
 cd 4.7/
 # edit wp-tests-config-sample.php -> wp-tests-config.php
 
 phpunit


Stock result:
::

 # Lots of failure related to db index size, tests reworked after 4.7
 FAILURES!
 Tests: 7909, Assertions: 33515, Failures: 22, Skipped: 97.


Documented but doesn't seem useful:
::

 apt install npm -t stretch-backports
 apt install grunt
 npm install
 grunt build


Test Debian's code:
::

 (cd ../wordpress-4.7.18+dfsg/ && quilt push -a)
 mv src src.bak
 cp -a ../wordpress-4.7.18+dfsg src
 rm -rf src/debian/
 cp -a src.bak/wp-content/themes/twentyeleven/ src/wp-content/themes/
 
 phpunit
 # +1 error because of debian/patches/010disabling_update_note.patch


Single test:
::

 # import test from trunk changeset if needed
 phpunit tests/phpunit/tests/formatting/redirect.php


-------------
Basic install
-------------

::

 debi
 apt install mariadb-server exim4


Cf. /usr/share/doc/wordpress/examples/setup-mysql + /usr/share/doc/wordpress/examples/apache.conf .

Account creation: Admin > Settings > General > Membership > Anyone can register

Multisite: Admin > Tools > Network setup

--------
See also
--------

 * https://make.wordpress.org/core/handbook/testing/automated-testing/phpunit/#running-the-test-suite
 * https://make.wordpress.org/hosting/test-results-getting-started/
 * https://github.com/WordPress/phpunit-test-runner


| Copyright (C) 2020  Sylvain Beucler
