=======
FreeRDP
=======

FreeRDP provides a Windows Remote Desktop client.

You'll probably need Windows VMs, including old ones. You may be
interested in https://archive.org/details/modern.ie-vm and
https://developer.microsoft.com/en-us/windows/downloads/virtual-machines/

Basic testing for W7:
::

  xfreerdp /v:192.168.122.41 /u:ieuser /p:'Passw0rd!' /sec:rdp

USB testing (``/usb:id,dev:XXXX:XXXX`` with XXXX from ``lsusb``):
::

  xfreerdp ... /usb:id,dev:0781:5581

Note: to share USB devices through a VirtualBox Debian VM (so you can
share them to e.g. xfreerdp/stretch), you'll need your main desktop
user to be in the ``vboxusers`` group.

Directory share testing (test both reading and writing):
::

  xfreerdp ... /drive:myhome,/home/myaccount
  # access it through \\tsclient\myhome

| Copyright (C) 2023, 2024  Sylvain Beucler
