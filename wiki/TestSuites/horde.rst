=====
Horde
=====

Installation: see `Horde <https://wiki.debian.org/Horde>`_.

Typically
::

 # mysql setup
 apt install php-horde-webmail
 webmail-install
 # admin user setup

----------
Test suite
----------

For a single plugin package:
::

 apt install autopkgtest
 adt-run -B ./ --- null  # jessie
 autopkgtest -B -- null  # stretch

or manually:
::

 apt install php-horde-test
 quilt push -a
 cd Horde_Text_Filter-*/test/Horde/Text/Filter/
 phpunit .


| Copyright (C) 2021  Sylvain Beucler
