=====
putty
=====

This simple test cover generating a key with putty, and using it to
authenticate against an SSH server. The key generated in this example is a
521-bit ECDSA key, that suffered from
`CVE-2024-31497 <https://deb.freexian.com/extended-lts/tracker/CVE-2024-31497>`_.

.. code:: sh

   puttygen -t ecdsa -b 521 -o key521
   puttygen key521 -O public-openssh >> .ssh/authorized_keys
   eval $(pageant -T)
   pageant -a key521
   plink -agent localhost

| Copyright (C) 2024  Santiago Ruano Rincón
