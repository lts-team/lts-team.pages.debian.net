=======
orthanc
=======

----------
Test suite
----------

orthanc can be tested using docker. Create a running docker container and
expose the port 8042. Also mount the directory /mnt to the docker container
to be able to access the test data.


::
docker run -p 8045:8045  --rm -it --mount type=bind,source=/mnt,target=/mnt  debian:buster



After the container is running, one need to install the orthanc server. But before
some updates should be done in the configuration file. Install also vim or any other
console text editor.


::

  apt update && apt -y dist-upgrade && apt install orthanc vim
  vim /etc/orthanc/orthanc.json

Change "RemoteAccessAllowed" to true. Start the server.

::

  /usr/sbin/Orthanc --config /etc/orthanc/orthanc.json --verbose


After that the service can be accessed from the host machine using the port 8042.

::

  curl -X GET http://localhost:8042/instances -v

If no answer comes from the server, try to use the credentials. The default user is orthanc
and the password is orthanc.

::

  curl -X GET http://localhost:8042/instances -u "orthanc:orthanc" -v

Download the dcm file and upload it to the server.

::

  curl -X POST http://localhost:8042/instances --data-binary @test_img.dcm -u "orthanc:orthanc" -v

The server should answer with a json object containing the id of the uploaded file.

To test the export the file on the server, use the following command.

::

  curl -X POST http://localhost:8042/instances/____ID____FROM____JSON____/export -u "orthanc:orthanc" --data "/tmp/test_exported.dcim" -v


This test case was created to test CVE-2023-33466
