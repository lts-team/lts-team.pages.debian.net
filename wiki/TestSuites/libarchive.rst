==========
libarchive
==========

The test suite is not run by default, you can force it with:
::

 $ dpkg-reconfigure locales
 # -> make sure to enable en_US.UTF-8
 
 $ DEB_BUILD_OPTIONS=check debuild
 # (around 5mn)


| Copyright (C) 2022  Sylvain Beucler
