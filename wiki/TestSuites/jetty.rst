=====
jetty
=====

----------------
Basic deployment
----------------

::

 apt install tomcat8-docs
 cp -a /usr/share/tomcat8-docs/docs/appdev/sample/sample.war /var/lib/jetty9/webapps/
 sensible-browser http://127.0.0.1:8080/sample/

-------
Caveats
-------

*debian/* gets corrupted on build, see DebianBug:807686.
Make backups.

---
TLS
---

Edit */etc/jetty9/start.ini* and add ssl+https modules:
::

 --module=deploy,http,jsp,jstl,websocket,ext,resources,ssl,https

Jetty will listen on :8443 with a self-signed certificate.

--------
Embedded
--------

::

 debuild
 cd tests/test-webapps/
 mvn
 curl -L repo.jenkins-ci.org/public/org/jenkins-ci/jenkins/1.26/jenkins-1.26.pom > ~/.m2/repository/org/jenkins-ci/jenkins/1.26/jenkins-1.26.pom
 mvn
 cd jetty-distribution/
 mvn
 cd examples/embedded/
 service jetty9 stop
 java -cp target/classes:$(ls /usr/share/java/*.jar|tr '\n' ':') org.eclipse.jetty.embedded.ManyServletContexts
 sensible-browser http://127.0.0.1:8080/it/

------
jessie
------

jetty (v6.x) is only provided as a library (use jetty8 for a full environment).
One way to test is using the embedded tests:
::

 debuild
 cd examples/embedded/
 mvn -DrunEmbedded test
 cp -aL /usr/share/java/jetty.jar ~/.m2/repository/org/mortbay/jetty/jetty/6.1.26/jetty-6.1.26.jar
 cp -aL /usr/share/java/jetty-util.jar ~/.m2/repository/org/mortbay/jetty/jetty-util/6.1.26/jetty-util-6.1.26.jar
 cp -aL /usr/share/java/jetty-sslengine.jar ~/.m2/repository/org/mortbay/jetty/jetty-sslengine/6.1.26/jetty-sslengine-6.1.26.jar
 mvn --offline -DrunEmbedded test

 
| Copyright (C) 2021  Sylvain Beucler
