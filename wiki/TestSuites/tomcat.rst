======
tomcat
======

-----------------
Run a single test
-----------------

The full test suite requires ~45mn. You can run one single test with:
::

 ant ... test -Dtest.name='**/TestSendFile*'

You can run ant manually or temporarily edit *debian/rules*.

---------------
Skip test suite
---------------

*export DEB_BUILD_OPTIONS=nocheck*

Technicalities aside, if the current version doesn't pass the test suite, one may skip the test suite for new uploads, provided no additional failures occur (`see also <https://lists.debian.org/debian-lts/2019/08/msg00014.html>`_).

-------------
Smoke testing
-------------

https://lists.debian.org/debian-lts/2019/08/msg00014.html


| Copyright (C) 2019  Sylvain Beucler
