====
ruby
====

Testsuite
---------

The testsuite is run on build.  Some tests fail if run as root:
compile as an unprivileged user and use ``autopkgtest --user``.

To run a subset from the currently built ruby:
::

 tool/runruby.rb test/runner.rb 'test/net/*'
 tool/runruby.rb test/runner.rb -n 'TestNetHTTPS#test_session_reuse_but_expire'
 tool/runruby.rb test/runner.rb -n 'TestGemSpecification#test_date_equals_time' test/rubygems/test_gem_specification.rb

In a GMT+X timezone, ``TestGemSpecification#test_date_equals_time`` is
always failing (with a 1-day mismatch), from stretch to bookworm.
This doesn't trigger in a typical buildd/pbuilder environment. Build
with TZ=UTC as a work-around.

ruby2.7/bullseye will produce many errors for
``test/ruby/test_time_tz.rb`` if ``ruby-tzinfo`` is installed.
Uninstall it first.


Build caveats
-------------

``ruby2.x`` appears to be usually built with:
::

  gbp buildpackage ... --git-builder=git-pbuilder

If you want to have a working compiled directory, e.g. to quickly run
and fix individual tests, this gets tricky.

Building from Git (without the tarball timestamps), or rebuilding
twice (as the first build enables `make distclean`), triggers a
different build path which requires "host ruby" to be already
installed, or files to be regenerated with different versions of tools
such as ``bison``. If you don't want that:
::

 tar --strip-components 1 -xf ../ruby2.*.orig.tar.*
 # or
 touch prelude.c miniprelude.c id.* probes.dmyh *.inc parse.c enc/trans/newline.c  # buster
 
 gbp buildpackage ...

Even without that, building leaves modified and extra files, making it
hard to reset to a clean state in a ``dpkg-source`` extract, so better
use ``gbp``. Note that buster/bullseye's ``gbp buildpackage`` silently
exits if ``pristinetar`` is not present, so install it.


Freexian CI
-----------

`autodep8
<https://manpages.debian.org/unstable/autodep8/autodep8.1.en.html>`_
is installed and triggers ``gem2deb-test-runner`` while testing rdeps
Ruby packages; this adds generic tests even if there's no
``debian/tests/``.

``autodep8`` was forked to support jessie, see also
`ruby-rmagick autopkgtest fails #57
<https://gitlab.com/freexian/services/deblts-team/extended-lts/-/issues/57>`_.


| Copyright (C) 2024  Sylvain Beucler
