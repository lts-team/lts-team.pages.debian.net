===
ntp
===

(tested during ntp/bullseye backport to jessie)

-------------------
Built-in test suite
-------------------

::

 debuild
 # do it again manually
 make check

-----------
smoke tests
-----------

::

 service ntp start
 #ntpdc -c peers  # deprecated, now hangs, see #867227
 ntpq -c peers


::

 server1# service ntp start
 server1# iptables -I INPUT 1 -p udp --dport 123 -j ACCEPT
 
 server2# service ntp stop
 server2# date -s now-1mn
 server2# ntpdate -v server1


::

 server1# service ntp start
 server1# iptables -I INPUT 1 -p udp --dport 123 -j ACCEPT
 
 server2# # edit /etc/ntp.conf, comment out pools, add server server1
 server2# date -s now-1mn
 server2# service ntp start
 server2# ntpq -c peers
 # be very patient
 server2# date

---------
clknetsim
---------

 * https://mlichvar.fedorapeople.org/clknetsim/
 * https://github.com/mlichvar/clknetsim
 * https://github.com/mlichvar/ntptestsuite

Fix `Makefile`: `/usr/include/sys/time.h` -> `/usr/include/x86_64-linux-gnu/sys/time.h`.

::

 cd ntclknetsim/
 make
 cd ntptestsuite/
 PATH=/usr/sbin:$PATH ./testall.sh


Tests appear flacky but can trigger segfaults. Mostly works for jessie's ntp, but completely fails for buster & up.


| Copyright (C) 2021  Sylvain Beucler
