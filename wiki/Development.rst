===========
Development
===========

.. highlight:: shell

--------------------------------------
Shortcut: 7 steps for publishing a DLA
--------------------------------------
 *Debian Long Term Support Security Advisory (DLA)*

HOWTO prepare a security update for LTS for casual contributor:
 #. `Claim the issue in the security tracker (in dla-needed.txt)`_
 #. `Prepare the update`_
 #. `Test the update`_
 #. `Upload the update`_
 #. `Claim a DLA ID in DLA/list`_
 #. `Announce the update`_
 #. `Publish the Git repository and tags`_

----------------------------------------
Contributing to Debian Long Term Support
----------------------------------------

The Debian LTS team is always looking for more volunteers to do a better job. With more resources, we could for example:
 * support packages that are currently unsupported
 * take care of all Debian packages and not only the most popular ones
 * fix lower priority issues

If you want to get involved in the LTS team and help keep Debian packages secure for 5 years, have a look at this page. We assume that you are already familiar with the repository layout described in `LTS/Using <https://wiki.debian.org/LTS/Using>`_ and that you are subscribed to the LTS mailing list: http://lists.debian.org/debian-lts/

You can help in many ways:

.. contents:: :depth: 4

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
Test updated packages and report regressions
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

As a simple user, you can test packages that have been updated (or that are in the process of being updated). If you find a regression (i.e. something that used to work and that no longer works), then please report it to debian-lts@lists.debian.org and put the person who prepared the update in copy (in case they are not subscribed to the list).

Many LTS contributors are looking for testers for their updated packages. They send call for tests on the mailing list, so please `subscribe to it <http://lists.debian.org/debian-lts/>`_ and test the packages they provide when you can, and report back whether they work for you.

~~~~~~~~~~~~~~~~~~~~~~~
Debian Security Tracker
~~~~~~~~~~~~~~~~~~~~~~~

The Debian LTS team makes extensive use of the Debian Security
Tracker.  This is a database of all known security issues in Debian.

 * `Git repository <https://salsa.debian.org/security-tracker-team/>`_
   at Salsa. This is the central data and tooling. DDs automatically
   have commit access, others may `request
   <https://lts-team.pages.debian.net/wiki/Contact.html>`_ membership.
 * `Documentation <https://security-team.debian.org/security_tracker.html>`_:
   **important**, please do read, to understand the triage and
   update logic.
 * `Web interface <https://security-tracker.debian.org/tracker/>`_:
   reflects the changes every 30mn or so.
 * `Commit notifications
   <https://alioth-lists.debian.net/cgi-bin/mailman/listinfo/debian-security-tracker-commits>`_.
 * `debian-security-tracker@l.d.o
   <https://lists.debian.org/debian-security-tracker/>`_: not useful
   in the context of LTS.

LTS developers must keep the security tracker up-to-date. In
particular:

 * `data/CVE/list <#cve-status-list-for-lts>`_: large, single-file
   text-base database of all current and past CVEs, for non-LTS and
   LTS Debian dists. Includes links to related bug reports, upstream
   commits, fixing versions, exploits, and any information useful to
   the Security and LTS teams.

 * `data/dla-needed.txt
   <#claim-the-issue-in-the-security-tracker-in-dla-needed-txt>`__:
   LTS work queue.

 * `data/DLA/list <#claim-a-dla-id-in-dla-list>`_: semi-automated
   reference list of DLAs with affected packages and CVEs.

To work on the security tracker, clone the repository.
::

 git clone git@salsa.debian.org:security-tracker-team/security-tracker.git
 cd security-tracker/
 sudo apt install jq python3-apt
 bin/setup-repo

.. note::

  The git repository is heavy and a first normal clone will easily
  take an hour. For occasional work, a shallow clone (with ``--depth
  1``) will only take a few minutes.

.. attention::

  Please run ``bin/setup-repo``, which installs a pre-commit hook to
  validate syntax (and avoid breaking web interface updates).

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
Prepare security updates for LTS
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
Claim the issue in the security tracker (in dla-needed.txt)
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

In order to prevent duplication of effort, make sure the issue is
listed in the `data/dla-needed.txt
<https://salsa.debian.org/security-tracker-team/security-tracker/blob/master/data/dla-needed.txt>`_
file from the `security tracker <#debian-security-tracker>`_ and add
your name to it.  Normally, packages are triaged by Front Desk in
``dla-needed.txt`` first.

``dla-needed.txt`` reflects LTS-specific information, such as who is
working on a package, requests for help, or problems encountered.

Do not add packages to ``dla-needed.txt`` (bypass front desk by
self-triaging) when you are not at front desk.  The exception is when
front desk is unresponsive (for more than 1-2 working days) or when
the issue is really critical and timely release is important
(e.g. embargoed issues).  In that case, notify front desk about it,
and document the bypass reason in the git commit message (for
clarity to FD and other contributors).

As mentioned in the previous section, make sure to also review and update the relevant information in ``data/CVE/list`` when working on a package for LTS work.

**You shall only claim it for the time you do active work. You are expected to actively handle
the problem within 2-3 days and then you are expected to announce your intention to upload so
others can test it for a few (3-4) more days. If you can not solve the problem within the 2-3 days,
then please mention why it takes more time, how far you have gone, the preliminary results and/or
free it for someone else to work on it. We do not want people to work on a package for a few hours,
wait a few days, work again on it, wait more and so on.**

Edit the data/dla-needed.txt file:
::

 $ editor data/dla-needed.txt
 $ git commit -m "Claim foo in dla-needed.txt" data/dla-needed.txt
 $ git push

If the package you worked on got unclaimed by the coordinator after 2
weeks of apparent inactivity, and you want to re-claim it, do not only
restore your name/claim, but also add information about your progress
and/or difficulties. Please add a new NOTE line with the current date,
and detail the progress status (not just "WIP", for instance "waiting
for feedback from maintainer" or "fixing multiple testsuite
failures"). That way the coordinator and other contributors can see
whether you're e.g. just facing lengthy work, lacking time (and could
hand over your work to another contributor), or could use additional
help.


""""""""""""""""""""""""""""
Special case: can't be fixed
""""""""""""""""""""""""""""

If a CVE is too hard to fix, first unclaim the package in
``dla-needed.txt`` and leave a note describing the approach and difficulties.

Then, depending on the difficulties and issue severity, either:

#. Leave the package for someone else to attempt.
#. If severity is low, or a work-around exists, tag the issue
   ``<ignored>`` (cf. `CVE status list for LTS`_)
   then drop the package from ``dla-needed.txt``:

   .. code-block:: text

     [bullseye] - SRCPKG <ignored> (Minor issue; Easily worked around by ... hard to backport due to ...)
#. If the issue is high-severity, a newer version of the package may
   need to be backported. Discuss this at debian-lts@lists.debian.org
   and open a `backport proposal at lts-updates-tasks
   <https://salsa.debian.org/lts-team/lts-updates-tasks/-/issues/new?issuable_template=Propose%20full%20version%20backport>`_.


""""""""""""""""""
Prepare the update
""""""""""""""""""

**Git repository:** your work should be published either in the
package maintainers' repository, or in a fork on Salsa. See
:doc:`../git-workflow-lts` for details.

**Backport the fixes** to the version in LTS.

Make sure that each fix was approved by the upstream project
(e.g. committed to their development repository), sometimes early
patch proposals are erroneous or incomplete.

If there is no fix, it is fine develop a new one: be a good citizen
and propose the patch back upstream, `file a bug report
<#contacting-the-maintainers>`__ for the CVE if not already, reference
your patch in that report and link it in the security tracker.

(If the issue being addressed requires *AddressSanitizer* in order to
reproduce the vulnerability and confirm the fix, then check
:doc:`../howtos/lts-Development-Asan`).

If the fix comes with new binary files (e.g. test cases), they won't
be accepted by quilt, but you can reference them in
``debian/source/include-binaries`` and they'll be included in the
``.debian.tar.xz`` file directly (`discussion
<https://lists.debian.org/debian-lts/2021/02/msg00008.html>`_).
When working with old (1.0) format, ``include-binaries`` is not
supported, so either store a base64 version of the file that you'll
unpack on test or build, or try to make a patch with ``diff -a`` (not
compatible with quilt refresh, but works with push/pop).

**debian/changelog entry:** initialize the entry with the text
"*Non-maintainer upload by the LTS Security Team.*", which you can do
with ``dch --lts`` (`since buster
<https://bugs.debian.org/762715>`_). Do not use ``-s`` and otherwise
ensure it doesn't look endorsed by the Debian Security Team.

Set the target distribution to "**bullseye-security**".

The versioning follows the conventions already used at
security.debian.org:

 * If a package already e.g. had a ``+deb11u1`` update, use
   ``+deb11u2`` for the next update.
 * If a package hasn't seen an update, use ``+deb11u1`` for the next
   update.
 * For the rare case where a new upstream version is introduced:
    * Make sure to use a lower version than in bookworm, such as
      ``-0+deb11u1`` or ``~deb11u1``, see `this discussion
      <https://lists.debian.org/debian-lts/2020/06/msg00008.html>`_.
      Just don't append ``+deb11uX`` as this will cause upgrade
      problems!
    * Reset to deb11\ **u1**, regardless how many uploads were
      previously done.
 * Note: historically codenames have been used as version suffixes
   (e.g. *+squeezeX*), but this was changed as dist versions can be
   sorted.

When working on multiple dists, *do not copy/paste the changelog
date*, as it sets `SOURCE_DATE_EPOCH
<https://reproducible-builds.org/specs/source-date-epoch/>`_. This
makes it more likely to ship files with the same date and the same
size, but different content, across multiple dists. This needlessly
confuses tools such as ``rsync``. We suggest using ``dch
-r/--release`` to finalize the changelogs and use a unique date.

BTS numbers can be referenced but are currently `not processed <https://lists.debian.org/debian-lts/2020/02/msg00048.html>`_.

Note that for non-native packages, first uploads to security-master need to
include the source tarball (even if it's already present in the distribution
you're uploading to) by using ``dpkg-buildpackage -sa``; check at
`<http://security.debian.org/pool/>`_ or try ``rmadison -u qa PACKAGE``.

**Building:** we want a clean and isolated build, close to the Debian
buildd setup, to identify build issues before the upload.

Salsa: if you're using salsa-ci and :doc:`lts bullseye pipeline
<../git-workflow-lts>`, Salsa can build your package for many
architectures. It uses combined indep+arch though, unlike buildds, and
can be hard to debug.

Manually: we recommend using
`sbuild <https://wiki.debian.org/sbuild>`_
/`pbuilder <https://wiki.debian.org/pbuilder>`_.
This is efficient when debugging build issues. For instance:

::

 # Init
 sudo pbuilder create --basetgz /var/cache/pbuilder/base-bullseye.tgz \
  --distribution bullseye \
  --othermirror 'deb http://security.debian.org/ bullseye-security main contrib'
 sudo pbuilder update --basetgz /var/cache/pbuilder/base-bullseye.tgz

 # Rebuild source packages _from bullseye_ (in extracted source)
 pdebuild --buildresult .. --use-pdebuild-internal --debbuildopts '-S' \
  -- --basetgz /var/cache/pbuilder/base-bullseye.tgz
 # doesn't work: sudo pbuilder debuild --basetgz /var/cache/pbuilder/base-bullseye.tgz --buildresult .. --debbuildopts '-S'
 # or: just 'debuild' in a bullseye VM

 # Rebuild binary packages from bullseye,
 # reproducing buildd's separate build-indep/build-arch + no network
 export DEB_BUILD_OPTIONS="nocheck ..."
 # - first security upload:
 sudo --preserve-env=DEB_BUILD_OPTIONS \
  pbuilder build --basetgz /var/cache/pbuilder/base-bullseye.tgz \
   --source-only-changes --logfile build-indep.log --buildresult . \
   --binary-indep --debbuildopts '-sa' package+deb11u1.dsc
 sudo --preserve-env=DEB_BUILD_OPTIONS \
  pbuilder build --basetgz /var/cache/pbuilder/base-bullseye.tgz \
   --source-only-changes --logfile build-arch.log  --buildresult . \
   --binary-arch  --debbuildopts '-sa' package+deb11u1.dsc
 # - later uploads (source tarball already present at security.d.o):
 sudo --preserve-env=DEB_BUILD_OPTIONS \
  pbuilder build --basetgz /var/cache/pbuilder/base-bullseye.tgz \
   --source-only-changes --logfile build-indep.log --buildresult . \
   --binary-indep package+deb11u2.dsc
 sudo --preserve-env=DEB_BUILD_OPTIONS \
  pbuilder build --basetgz /var/cache/pbuilder/base-bullseye.tgz \
   --source-only-changes --logfile build-arch.log  --buildresult . \
   --binary-arch  package+deb11u2.dsc

 # to debug a failed build:
 # https://pbuilder-docs.readthedocs.io/en/latest/faq.html#logging-in-to-pbuilder-to-investigate-build-failure
 mkdir hooks/
 cp -a /usr/share/doc/pbuilder/examples/C10shell hooks/
 sudo pbuilder ... --hookdir hooks/ ...
 # during build, network is disabled with 'unshare -n' + 'ifconfig lo up'

See also: `Debian Developer's Reference - Preparing packages to
address security issues
<https://www.debian.org/doc/manuals/developers-reference/pkgs.html#preparing-packages-to-address-security-issues>`_


"""""""""""""""
Test the update
"""""""""""""""

:doc:`Salsa-CI <../git-workflow-lts>` can run many tests for you. Pros:
this is automatic when working from Salsa Git. Cons: failures are hard
to debug; some tests are currently (2025-02) run with unstable
versions of the tools, rather than LTS'.

Don't just run the automated tests. You should have an LTS VM to
install the package in; perform at least basic ("smoke") testing, make
targetted testing for the features you patched, check for regression
in reverse-dependencies.

See :doc:`TestSuites` for package-specific test instructions.

**Test suites:** if there's a built-in test suite, make sure it runs fine at build-time in the LTS environment.
It is acceptable to enable or even backport test suites from later releases if they are available.
Conversely push your testsuite fixes and improvements to unstable or even stable.

DEP-8 tests in ``debian/tests/``, if any, can be run and debugged
manually with :doc:`TestSuites/autopkgtest`.

**Reverse-dependencies:** identify them with ``find-rdeps``,
`apt-rdepends <https://packages.debian.org/apt-rdepends>`_ ``-r``,
`dose-ceve <https://manpages.debian.org/bullseye/dose-extra/dose-ceve.1.en.html#EXAMPLES>`_
from `dose-extra <https://packages.debian.org/dose-extra>`_).
Install some of them, to check if your package still runs correctly
with them. Running their own DEP-8 tests like `ci.debian.net
<https://ci.debian.net/>`_ helps too.

**Other architectures:** it is worth testing with both a 32- and
64-bit architecture, see e.g.  `CVE-2019-14866
<https://lists.debian.org/debian-lts/2019/11/msg00005.html>`_.

To debug a build in an architecture you don't have access to, you may
use a porterbox, see `PorterBoxHowToUse
<https://wiki.debian.org/PorterBoxHowToUse>`_.

**Source checks:** inspect overall source changes since last release,
even if you used Git:
::

 debdiff package+deb11u3.dsc package+deb11u4.dsc | diffstat
 debdiff package+deb11u3.dsc package+deb11u4.dsc | less

If possible check the binaries too:
::

 debdiff --from bullseye-old/*.deb --to bullseye-new/*.deb

**Generic checks:** those are made by Salsa, but running them manually
can help debugging, and you can use a version closer to (E)LTS:
::

 # check for common packaging issues in last build
 # from extracted source after build, bullseye host (only check new errors)
 lintian -i

 # test package upgrade
 sudo piuparts -d bullseye \
   --extra-repo='deb http://security.debian.org/ bullseye-security main' \
   -l piuparts-package.log \
   -I :etc/buggy-dep \
   --single-changes-list package+deb11u4_{all,amd64}.changes \
   | grep -P '(INFO|ERROR):'
 # also consider --install-remove-install

**Reviews**: it's encourage to seek reviews from interested parties
(such as upstream authors, Debian maintainers, security team, LTS
team, etc.). If you ask for reviews, you should supply the following
information:

 1. A generated debdiff against the previous version in LTS.
 2. A link to a test package that can be downloaded and tested.

It is common for LTS workers to provide packages through Salsa
artifacts, or to upload to a personal repository such as
`people.debian.org
<https://www.debian.org/doc/manuals/developers-reference/ch04.en.html#servers-people>`_.
A few guidelines for those temporary uploads:

 * sign the packages (using ``debsign``, for example) so that users can verify package integrity
 * make sure to have the target distribution set to UNRELEASED so that nobody else can upload them before they are ready.

"""""""""""""""""
Upload the update
"""""""""""""""""

*Uploads to Debian LTS* **do not wait** *for a DLA or any other manual intervention before being installed into the Debian Archive.  In this regard, they are different from stable security uploads.*

If you're satisfied with your tests, upload to security-master, using `dput <https://tracker.debian.org/pkg/dput>`_ or `dput-ng <https://tracker.debian.org/pkg/dput-ng>`_ :

::

 dput security-master hello_1.0-1+deb11u1_source.changes

Once the upload is accepted, an email will be sent to you and to the
`debian-lts-changes <https://lists.debian.org/debian-lts-changes>`_
mailing list, confirming the update has been released into the Debian
archive. The `BTS` bot will also announce it in the
`#debian-devel-changes <ircs://irc.oftc.net/debian-devel-changes>`_
channel. (wait for ~30mn)

The package will then be auto-built for all the available architectures.
Check the build logs at:

  `<https://buildd.debian.org/status/package.php?suite=bullseye-security&p=SOURCE_PACKAGE>`_

Troubleshooting:

**Built-Using refers to non-existing source package**: if your package is rejected with this reason, open a bug against package **ftp.debian.org** requesting to add the missing package, see e.g. `#974877 <https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=974877>`_ or `#974954 <https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=974954>`_.
For the same reason, the package may also be accepted but then
unexplicably stalls in the "Uploaded" (not "Installed") stage in the
buildd status (`#1033604 <https://bugs.debian.org/1033604>`_).

**Non-free packages**: currently (2022-01-19) buildds won't build
source-only uploads to security-master (a bug, according to Aurélien
Jarno), impacting packages such as `amd64-microcode
<https://packages.debian.org/amd64-microcode>`_ and
`firmware-linux-nonfree
<https://packages.debian.org/firmware-linux-nonfree>`_.  Instead,
upload the full "source-code" + the amd64 binary packages (build with
``-sa`` and upload the ``_amd64.changes`` file); then upload only the
i386 binary packages (``dpkg-buildpackage -b``; or use ``changestool``
from `reprepro <https://packages.debian.org/reprepro>`_ to exclude
unwanted files from the changes file).

**Flaky build**: see `Requesting wanna-build actions
<https://release.debian.org/wanna-build.txt>`_ if your package failed
to build and you need a gb/giveback/give-back. Only do this if the
build failure can't actually be fixed cleanly.

""""""""""""""""""""""""""
Claim a DLA ID in DLA/list
""""""""""""""""""""""""""

Once your upload has been built successfully, you can go ahead with the DLA.

Please make an effort to send out announcement for anything released
via security.debian.org with not too much delay; there's all kinds of
processes by people out there who react on those emails and there's
also people who get suspicious if an update is available without a
corresponding announcement.

Run ``bin/gen-DLA`` in the top directory of the Git repository. It automatically generates an entry in ``data/DLA/list`` and asks you to commit the changes to ensure that no IDs are used twice. The following command would add an entry for src:hello fixing CVE-2014-0666 and creates an advisory template in the top directory of your security-tracker checkout.

::

 $ DEBFULLNAME=xxx bin/gen-DLA --save hello CVE-2014-0666
 # OR
 $ DEBFULLNAME=xxx bin/gen-DLA --save .../hello_1.2-3+deb11u4_source.changes


That list of CVE will make sure that the security tracker is aware of the fix and mark the issues as resolved for that specific package.

``gen-DLA`` will automatically remove prior suite triage for the fixed vulnerabilities (e.g. ``[bullseye] - package <postponed>``). However, the ``DLA-XXXX-1`` annotation will be added by the bi-daily cron, and the fixed version will be inferred automatically (don't add it).

"""""""""""""""""""
Announce the update
"""""""""""""""""""

Only send the DLA to the mailing list when you have confirmed that the
package was processed and built on all supported architectures.

Send mail to the `debian-lts-announce
<https://lists.debian.org/debian-lts-announce/>`_ mailing list. The
mail needs to be signed by a GPG key in the debian or
debian-maintainers keyring. Inline signatures always work (PGP/MIME
check works with mutt but fails with Thunderbird).

The advisory template has been created by ``bin/gen-DLA`` (see before)
and generally looks like this:

.. code-block:: email

 Subject: [SECURITY] [DLA $DLA-1] $SOURCEPACKAGENAME security update
 
 Package        : $SOURCEPACKAGENAME
 Version        : $LTS_VERSION
 CVE ID         : CVE-2014-0001 CVE-2014-0002
 Debian Bug     : 12345
 
 DLA text goes here
 [...]

 
**In addition to describing the vulnerabilities, remember to add a short package description, so the audience can better understand the context and whether they are affected.**

Please also don't append your personal signatures to DLAs.

If you use mutt, a simple way to send it is to use
::

 mutt -e "unset signature" -H DLA-123

Simple ``mail`` command works:
::

 cat DLA-_BODY_.txt \
   | gpg --clearsign \
   | mail -s "[SECURITY] [DLA XXXX-X] $SOURCEPACKAGENAME security update" \
          -r "You <you@debian.org>" \
          debian-lts-announce@lists.debian.org

Official documentation:
 * Troubleshooting: `PGP-based approvals <https://wiki.debian.org/DebianMailingLists#PGP-based_approvals>`_
 * `Source code <https://salsa.debian.org/debian-listmasters/sigcheck/-/blob/master/sigcheck>`_
 * `[Stable] Debian Developer's Reference - Security Advisories <https://www.debian.org/doc/manuals/developers-reference/pkgs.html#security-advisories>`_
 * `[Stable] Sending out the announcement to debian-security-announce <https://wiki.debian.org/DebianSecurity/AdvisoryCreation/SecFull#Sending_out_the_announcement_to_debian-security-announce>`_

When all this is done, you should remove the package from other locations it was uploaded to in the test section, if any.


"""""""""""""""""""""""""""""""""""
Publish the Git repository and tags
"""""""""""""""""""""""""""""""""""

Cf. :doc:`../git-workflow-lts`.

You may verify using the coordinator's check:
::

  $ bin/package-operations --advisory dla --days=1 --check-dla-git-tag

""""""""""""""""""""""""""""""""""
Prepare regression updates for LTS
""""""""""""""""""""""""""""""""""

If an upload introduces a regression, it needs to be fixed as soon as possible.

"""""""""""""""""""""""""""""
Steps for a regression update
"""""""""""""""""""""""""""""

 1. Fix the package.
 2. Do a test build and test installation. Verify the regression issue and also the original issue are really resolved now.
 3. Make sure you use a clean chroot for building. Using `sbuild <https://wiki.debian.org/sbuild>`_/`pbuilder <https://wiki.debian.org/pbuilder>`_ is recommended.
 4. Upload the fixed package to bullseye-security.
 5. Use gen-DLA script to add a DLA entry to data/DLA/list and to provide you with an announcement email template. Two possible options:

   a. The regression fixes the previous upload of the package:

     ::

       $ bin/gen-DLA  --save $SOURCEPACKAGENAME regression

   b. The regression fixes an earlier than the previous upload of the package:

     ::

       $ bin/gen-DLA  --save $PREVIOUS_DLA_ID $SOURCEPACKAGENAME regression
       # /!\ $PREVIOUS_DLA_ID needs to be incremented (-1 -> -2)...

 6. If this is a security regression (e.g. incomplete or missing fix), reference the CVE ID in the ``data/DLA/list`` entry, otherwise (functional regression) do not reference it. 
 7. Wait for the uploaded package to arrive in the bullseye-security repository.
 8. In the base folder of the security-tracker Git repository, you can find an auto-generated LTS announcement template file with name ``DLA-$DLA-{2,3,4...}``. Use this file as the template for your regression fix announcement. Don't commit this file to Git(!). Copy+paste the announcement template to your email application, complete/edit it manually and finally send the regression fix DLA email to debian-lts-announce@lists.debian.org.
 9. Note: if a DLA requires a fix in another package, use the same DLA major number (see `this mail <https://lists.debian.org/debian-lts/2019/12/msg00072.html>`_)

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
Prepare other (non-security-related) updates for LTS
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Same procedure as a security update, but it is recommended to edit the title of the announcement e-mail accordingly, for instance:

 * https://lists.debian.org/debian-lts-announce/2020/12/msg00010.html "switch to ACMEv2 API"
 * https://lists.debian.org/debian-lts-announce/2020/12/msg00040.html "new upstream release"

~~~~~~~~~~~~~~~~~~~~~~
Triage security issues
~~~~~~~~~~~~~~~~~~~~~~

The Security and LTS teams cooperate to process the flow of new
security issues. Each is assigned a CVE number, so we often call this
"CVE triage".

.. caution::

  You must first get familiar with the `Security Tracker
  <#debian-security-tracker>`_ before committing.

"""""""""""""""""""""""
CVE status list for LTS
"""""""""""""""""""""""

.. important ::

  The reference documentation for ``data/CVE/list`` classification
  remains the `security tracker documentation
  <https://security-team.debian.org/security_tracker.html>`_. Do read
  it first.

There are some differences between LTS and Debian Security triaging:
 * LTS currently does not have any point releases.
 * The LTS Team prepares the vast majority of updates (few maintainers
   want to help with the LTS fixes).

In the context of LTS (currently ``[bullseye]``), here's a break-down of
each possible CVE status:

 * ``<postponed>`` when it's a minor issue that is not critical to
   address immediately, but may be fixed later.
 * ``<ignored>`` when there is a valid/strong reason *not* to fix it
   (e.g. it is only in sources neither compiled nor shipped in binary
   packages; it's non-critical and too hard to backport; it's
   non-critical and requires an API change affecting reverse
   dependencies).
 * ``<not-affected>`` when LTS version is not affected (e.g. because
   the vulnerable code was introduced later). Good practice: find and
   reference the related commit (in "NOTE: Introduced by ..."),
   because vulnerable code often gets moved around between upstream
   releases. If all versions are unaffected, tag at the package-level
   (e.g. ``- python3.7 <not-affected>``, without ``[bullseye]``).
 * ``<end-of-life>`` when the package is not supported in the LTS
   release (bullseye unsupported packages are listed at
   `debian-security-support:security-support-ended.deb11
   <https://salsa.debian.org/debian/debian-security-support/blob/master/security-support-ended.deb11>`_). If
   needed, update that file and add debian-security-support to
   ``dla-needed.txt`` -- cf. `debian-security-support:README.source
   <https://salsa.debian.org/debian/debian-security-support/-/blob/master/debian/README.source>`_.)
 * *specific version*: any later version is considered fixed. Normally
   not used, in favor of the automated ``{DLA-XXXX-X}`` tags.
 * ``<undetermined>``: rare, set at the package level by secteam, we
   don't use this tag, but you can help find more information.

.. attention::

  <no-dsa> is often used by the Security Team to indicate a first
  level of triage: no immediate DSA (aka package update), *but* maybe
  a delayed fix in the next point release, or just deferring to the
  maintainer [`doc <https://security-team.debian.org/triage.html>`_].

  We decided to **stop using <no-dsa>** starting 2024-03, due to its
  vagueness and ambiguity in the context of LTS, in favor of its
  `sub-states
  <https://security-team.debian.org/security_tracker.html#issues-not-warranting-a-security-advisory>`_
  <postponed> and <ignored>. Old no-dsa LTS entries can be considered
  <postponed>.

.. note::

  LTS triage normally tags dist-level (bullseye) entries, while higher,
  package-level tagging is reserved to secteam. We only tag at the
  package level for LTS-specific packages. Contact secteam if you have
  a recommendation for other package-level entries.

`Severity levels
<https://security-team.debian.org/security_tracker.html#severity-levels>`_
are normally set by the Security Team, coordinate with them if
needed. A couple notes:

 * "minor": the Security Team tends to mark non-Internet-facing
   vulnerabilities as ``<some tag> (Minor)`` with default severity,
   because those are not directly exploitable. However they allow
   greater penetration when the attacker put one foot in the victim's
   infrastructure and are better fixed in the long run.
 * unimportant: caution, makes the CVE disappear from all our tooling.
 * other states: rarely used in practice.

Hint at your thought process through a comment or a NOTE:

.. code-block:: text

  [bullseye] - package <postponed> (Minor issue, local OOM DoS)

  NOTE: PoC doesn't trigger in bullseye.

You may expand your rationale in the Git commit message, this helps
secteam review your changes, but makes it harder for others to notice
or search it later.

""""""""""""""""""""""""""""""
Initial package triage for LTS
""""""""""""""""""""""""""""""

This is normally done by `Front Desk <#front-desk-duties>`_ (see
`Claim the issue in the security tracker
<#claim-the-issue-in-the-security-tracker-in-dla-needed-txt>`_).

Guiding principles:
 * Reactivity.
 * Risks assessment (e.g. security vs. regression).
 * Reduce end users' (e.g. sysadmins) load with too many updates
   (daemon restarts, large network upgrades...).
 * Use the sponsors' funding efficiently.
 * Avoid confusing users with discrepancies between LTS and non-LTS
   dists.

Run ``bin/lts-cve-triage.py`` to gather a list of security issues in
the LTS release and group them by status.

The script includes sources which you may check manually:
 * Open issues from the tracker: `oldstable
   <https://security-tracker.debian.org/tracker/status/release/oldstable>`_/`oldoldstable
   <https://security-tracker.debian.org/tracker/status/release/oldoldstable>`_
   (tick the top-level checkboxes accordingly; "issues to be checked"
   means "undetermined").
 * Heuristic to detect explicit CVE fixes from new DSAs and stable
   point releases (may typically include batches of unmonitored
   ``<postponed>`` vulnerabilities), check
   `debian-security-announce@l.d.o
   <https://lists.debian.org/debian-security-announce/>`_ and
   `Proposed Updates
   <https://bugs.debian.org/cgi-bin/pkgreport.cgi?users=release.debian.org@packages.debian.org;tag=pu>`_
   / `debian-announce@l.d.o
   <https://lists.debian.org/debian-announce/>`_.
 * Package support status, see `debian-security-support
   <https://salsa.debian.org/debian/debian-security-support/>`__.

but doesn't handle:
 * non-CVE updates such as SUAs (tzdata, clamav...), do check
   `debian-stable-announce@l.d.o
   <https://lists.debian.org/debian-stable-announce/>`_.

and explicitly excludes:
 * Packages already added to ``dla-needed.txt`` (beware if they stay
   there for too long; check ``debian-lts:./find-work`` and the
   coordinators' weekly e-mail about *older than 21 days packages*).
 * Issues already triaged, in particular ``<postponed>``.
 * Issues with severity ``unimportant``.

Incidentally, consider subscribing to `openwall oss-security
<https://www.openwall.com/lists/oss-security/>`_ (``echo "subscribe" |
mail oss-security-subscribe@lists.openwall.com``). May be useful to
get reproducer and additional info.

""""""""""""""""""""""
Package/CVE evaluation
""""""""""""""""""""""

Triaging packages requires gathering information on the CVE, weighing
each piece, analyzing *carefully*, and making an educated
decision. This is not a yes/no process and is hardly automatable.

This includes the following criteria and considerations:

 * Package security Debian support status: supported, limited,
   ended/unsupported; see `debian-security-support
   <https://salsa.debian.org/debian/debian-security-support/>`__ +
   `non-free
   <https://www.debian.org/doc/debian-policy/ch-archive.html#the-non-free-archive-area>`_
   + we don't support games. Attention: unsupported packages may still
   be sponsored, contact the LTS Coordinator if that looks like an
   oversight.
 * LTS version vulnerable/not-affected: verify this during triage (if
   can be done in a reasonable amount of time).
 * Severity of the CVEs: critical issues are often embargoed and
   treated privately; 0-day exploits need to be fixed ASAP; most
   issues are non-critical.
 * Package criticality: security sensitivity (e.g. CLI tool
   vs. exposed Internet service); LTS sponsorship (means we should fix
   it, cf. ``debian-lts:./find-work``); size of user base (caution:
   popcon may *not* be a good indicator especially for private server
   components); number of reverse dependencies.
 * CVE processed in Debian: the CVE can be: reported to sid, fixed in
   sid, triaged by secteam, or affect a LTS-specific package; if
   non-critical and nothing happened yet, wait for a couple days
   rather than triage prematurely.
 * Package maintainers willing to do the update: see `Contact the
   maintainers <#contacting-the-maintainers>`__.
 * Fixability: patch availability (for dead or uncooperative
   upstreams, consider updating `debian-security-support
   <https://salsa.debian.org/debian/debian-security-support/>`__);
   backport complexity (regression risk, ability to backport a new
   upstream version without rdeps breakage, time requirement).
 * Number of pending CVEs: since LTS doesn't have point releases, we
   handle them when too many postponed issues piled up, or there is a
   more serious issue.
 * Date of the last package DLA: neither too frequent, to avoid
   impacting users (see guiding principles above); neither too
   infrequent, to avoid dragging too many changes (i.e. regression
   risks) at once.
 * Past triage: if we have consistently ignored certain type of CVEs
   for a certain package in the past, there's usually a good reason.
 * Alignment with other Debian dists: Security Team is taking similar
   decisions daily and we usually follow them. We should follow
   promptly if explicit fixes were published (DSA, stable/oldstable
   point release), or if a DSA is planned in
   dSa-needed.txt. Conversely we can offer to fix no-dsa/postponed
   issues in stable if we can fix them in LTS.
 * Vulnerability exposure in the press: this usually exaggerates
   severity to interested/worried users and prompts for more
   reactivity.

There are only 2 possible outcomes:

 * The CVE does not trigger an immediate package update and is
   `triaged <#cve-status-list-for-lts>`_.
 * The package is added to ``dla-needed.txt`` so that someone else
   prepares a package update and close the CVE(s).

In doubt, check with the other LTS contributors, the `package
maintainers <#contacting-the-maintainers>`_ or the Security Team. If
pressed for time, add the package to ``dla-needed.txt``, with a
``NOTE:`` about the doubt, and let another team member double-check
the vulnerability.

""""""""""""""""
Adding a package
""""""""""""""""

To add a package into ``dla-needed.txt`` or ``ela-needed.txt``, please
use the ``package-operations`` script from the `debian-lts repo
<https://freexian.gitlab.io/services/deblts-team/documentation/lts/information-for-lts-contributors.html>`_:
::

  bin/package-operations --lts  --add --package $packagename
  bin/package-operations --elts --add --package $packagename

This script creates a package record in ``packages.xml`` with central
info (like attention notes, maintainer being part of LTS team,
reference Git repository, etc.)  Don't pre-create the repository,
leave the details (raw import, fork, coordinate with DD repo...) to
whomever will prepare the update.

You can add an optional ``NOTE:`` to explain your thought process,
e.g.:

 * NOTE: 20201124: I believe the issue is severe but patches don't exist yet. (yourname/front-desk)
 * NOTE: 20201124: I believe the issue is minor but there are several other ignored and postponed issues already. Let's fix all of them now? (yourname/front-desk)
 * NOTE: 20240903: Follow fixes from bookworm 12.2 (3 CVEs) (yourname/front-desk)
 * NOTE: 20240312: CVE-2023-48733 fixed via DSA-5624-1 (yourname/front-desk)

""""""""""""""""""""""""""
Contacting the maintainers
""""""""""""""""""""""""""

**When (not) to contact**

* If the package maintainers are listed in
  `data/packages/lts-do-call-me
  <https://salsa.debian.org/security-tracker-team/security-tracker/-/blob/master/data/packages/lts-do-call-me>`_,
  you coordinate with them who will work on the update.
* The majority of maintainers are glad if security updates are handled
  by the LTS team, you may still contact them if necessary.
* If the package maintainers are listed in
  `data/packages/lts-do-not-call
  <https://salsa.debian.org/security-tracker-team/security-tracker/-/blob/master/data/packages/lts-do-not-call>`_,
  do not contact them.

**How to contact**

Add the package to ``dla-needed.txt`` and add a note when you have
contacted the maintainer. Then wait for a response and update the
notes, or remove the package if they feel no security update is
required.

If a vulnerability bug is unfixed in unstable and not reported in the
`BTS <https://wiki.debian.org/BTS>`_ yet, you can use `bin/report-vuln
<https://salsa.debian.org/security-tracker-team/security-tracker/raw/master/bin/report-vuln>`_
to open it. Discussions about severity and urgency are best done in
the bug report since all involved parties (LTS team, security team and
package maintainers) can get involved. Maintainers often know best how
severe an issue is and if an immediate update is actually needed.

Otherwise, the `bin/contact-maintainers
<https://salsa.debian.org/security-tracker-team/security-tracker/raw/master/bin/contact-maintainers>`_
has a few templates and recipients suggestions to help you (use
``--help`` for details): ::

 # Ask the maintainers if they want to update the package, otherwise we will
 $ bin/contact-maintainers --lts sudo CVE-2014-9680 CVE-2014-0106
 $ bin/contact-maintainers --lts --minor sudo CVE-2014-9680 CVE-2014-0106

 # Let the maintainers know we won't update (normally not needed)
 $ bin/contact-maintainers --lts --no-dsa sudo CVE-2014-9680 CVE-2014-0106

In order to avoid undue pressure on the maintainer, you must customize
the mail sent:

 * Always drop the list of uploaders and review/adjust the recipients
   list.
 * If the package was recently added to dSa-needed.txt, re-formulate
   the email to clarify this is Long Term Security and not the usual
   Security Team e-mail.
 * Check who was involved in making the Security Team aware of the
   issue. If it was the maintainer you need to re-formulate as well.

~~~~~~~~~~~~~~~~~
Front Desk duties
~~~~~~~~~~~~~~~~~

Front Desk is a shared role, in rotation. The allocations are managed
in ``org/lts-frontdesk.20XX.txt``, e.g. `lts-frontdesk.2023.txt
<https://salsa.debian.org/security-tracker-team/security-tracker/raw/master/org/lts-frontdesk.2023.txt>`_,
which itself is populated by the LTS Coordinator, depending on your
``frontdesk`` flag in LTS' and ELTS' ``contributors.yml`` files.

Front Desk work involves `CVE triage <#triage-security-issues>`_
and making sure queries on the `debian-lts@lists.debian.org <mailing
list>`_ or on IRC get an answer.  In particular, make sure to
follow-up on package upload sponsorship requests and questions from
new contributors to make sure things flow smoothly.  Following up on
stale and stalled issues is also a good idea.  Contributors should
feel free to contact Front Desk for any questions regarding LTS.

Front Desk is expected to perform their duties on a daily basis on
work days (Monday to Friday) at least, and preferably during
week-ends.

References:

* `Front desk 2015 announcement <https://lists.debian.org/debian-lts/2015/06/msg00032.html>`_


~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
Keeping track of LTS related bugs
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
We're tracking LTS related bugs in the Debian bug tracker using two usertags:

 * `ease-lts <https://bugs.debian.org/cgi-bin/pkgreport.cgi?tag=ease-lts;users=debian-lts@lists.debian.org>`_: Bugs that ease long term support like enabling autopkgtests or internal test suites
 * `infrastructure <https://bugs.debian.org/cgi-bin/pkgreport.cgi?tag=infrastructure;users=debian-lts@lists.debian.org>`_: LTS related issues in Debian infrastructure.

To add a certain usertag to a bug you can use the ''bts'' command:

::

 bts user debian-lts@lists.debian.org , usertags <bugnumber> ease-lts

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
Switching to the next LTS release
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

 * Send a reminder to debian-lts-announce@lists.debian.org two months before the official support for the last LTS ends.
 * Contact the `Teams/Publicity <https://wiki.debian.org/Teams/Publicity>`_ and draft an `announcement <https://wiki.debian.org/Teams/Publicity/Announcements>`_ that the old LTS release cycle has come to an end and a new one has started. `Example <https://salsa.debian.org/publicity-team/announcements/blob/eba424150df49898f5033e04602a828583a57488/en/2016/201602XX.wml>`_, `Example 2 <https://salsa.debian.org/publicity-team/announcements/commit/d816ef401c55297904868a4b8d0b7f18d5bc9154>`_
 * Contact the Stable Release Managers for coordinating the date of the last point release (and so that they can clean up the proposed updates queues).
 * Ensure that the infrastructure is ready for the new LTS release:
   * Contact the FTP master team to ask them to open the oldstable-security upload queue when we take over from the Security Team.
 * Contact the FTP team and ask them to wait four weeks with archiving the old LTS release to give users a "grace period" to update their systems.
 * Contact the Buildd team to ask them to shutdown the build queues for the old LTS release.
 * Request from the Security Team a copy of the oldstable build logs, remove from them the logs of non-LTS architectures, and ask the Wanna-build team to make them (the LTS-related logs) public. (`e-mail <https://lists.debian.org/debian-lts/2023/12/msg00024.html>`_)
 * Update all relevant pages especially `LTS/Using <https://wiki.debian.org/LTS/Using>`_, `LTS/Installing <https://salsa.debian.org/lts-team/lts-team.pages.debian.net/-/blob/master/wiki/Installing.rst>`_ and this `LTS/Development <https://salsa.debian.org/lts-team/lts-team.pages.debian.net/-/blob/master/wiki/Development.rst>`_ itself.
 * Check/update/merge security-support-ended.debX in `debian-security-support <https://tracker.debian.org/pkg/debian-security-support>`_

   * Follow debian-security-support's `README.source <https://salsa.debian.org/debian/debian-security-support/-/blob/master/debian/README.source>`_
   * We shouldn't just forward-port the EOL list, i.e. if the only reason to not support something is "we didn't support it in previous releases", then we should support it.
   * Also check for missing/pending EOLs announced in recent DSAs, e.g. ``grep 'discontinued' -r webwml/english/security/202*``
   * Request team members / sponsors opinion, e.g. `EOL candidates for security-support-ended.deb10 <https://lists.debian.org/debian-lts/2022/08/msg00001.html>`_
 * Update/verify the contents of `data/packages/lts-do-call-me` and `data/packages/lts-do-not-call` in the security tracker.

 * Reference `pending proposed-updates <https://bugs.debian.org/cgi-bin/pkgreport.cgi?users=release.debian.org@packages.debian.org;tag=pu>`_  in data/dla-needed.txt to prevent conflicts

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
Switching to the next Stable release
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

 * Update ``debian-archive-keyring`` for LTS (skipping one release
   during dist-upgrades is quite common); also needed as a work-around
   for `#992966 (simple-cdd)
   <https://lists.debian.org/debian-lts/2021/08/msg00037.html>`_ at
   the moment.

~~~~~~~~~~~~~~~~~~
Housekeeping Tasks
~~~~~~~~~~~~~~~~~~
There are several tasks where we base our work on the work of the security team and where we can support them - especially when there are no open issues in dla-needed.txt. This will lead to more packages to fix:

 * Easy: Add bug references for issues unfixed in unstable. Check `unreported issues in unstable <https://security-tracker.debian.org/tracker/status/unreported>`_, recheck them and file a bug if not already there. This makes sure we don't regress in unstable with bugs fixed in stable, oldstable. You can use the `bin/report-vuln <https://salsa.debian.org/security-tracker-team/security-tracker/raw/master/bin/report-vuln>`__ script to generate a template report to be further edited with reportbug or a MUA. In case you don't use reportbug to further submit the bugreport, please X-Debbugs-CC team@security.debian.org and secure-testing-team@lists.alioth.debian.org. This is done automatically if using reportbug.
 * Hard: Check `TODO items <https://security-tracker.debian.org/tracker/status/todo>`_ from the tracker. Be sure to only mark items as NOT-FOR-US when you're sure the software is not contained in Debian in any form.

------------
Tips & tools
------------

 * `Emacs shortcuts to deal with the security-tracker <https://github.com/agx/emacs-tools/blob/master/debian.el>`_
 * `TestSuites <https://wiki.debian.org/LTS/TestSuites>`_: tips to test difficult packages
 * `Fixing CVEs on Debian: Everything you probably know already
   <https://debconf24.debconf.org/talks/93-fixing-cves-on-debian-everything-you-probably-know-already/>`_:
   (DebConf24) beginner's overview of CVE processes and backporting tips for vulnerability fixes
