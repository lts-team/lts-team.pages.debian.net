###########
Test Suites
###########

.. toctree::
   :maxdepth: 2
   :hidden:
   :glob:

   TestSuites/*

Here are some tips on how to run some non-trivial package test suites, as well as some package-specific caveats.

If you add **a new test suite**, please add it also to the package
database in ``debian-lts/packages.yml``.

--------
Bullseye
--------

* :doc:`activemq <TestSuites/activemq>`

* :doc:`cacti <TestSuites/cacti>`

* :doc:`galera <TestSuites/galera>`

* :doc:`openvpn <TestSuites/openvpn>`

* :doc:`ruby <TestSuites/ruby>`

* :doc:`twisted <TestSuites/twisted>`

------
Buster
------

* :doc:`c-ares <TestSuites/c-ares>`

* :doc:`docker.io <TestSuites/docker.io>`

* :doc:`ffmpeg <TestSuites/ffmpeg>`

* `glib2.0* <https://lists.debian.org/debian-lts/2024/05/msg00008.html>`_

* :doc:`golang <TestSuites/golang>`

* :doc:`libarchive <TestSuites/libarchive>`

* :doc:`libgit2 <TestSuites/libgit2>`

* :doc:`lxc <TestSuites/lxc>`

* :doc:`nodejs <TestSuites/nodejs>`

* :doc:`openstack <TestSuites/OpenStack>`: general considerations on `OpenStack <https://wiki.debian.org/OpenStack>`_

* :doc:`orthanc <TestSuites/orthanc>`

* :doc:`putty <TestSuites/putty>`

* :doc:`pypy <TestSuites/pypy>`

* :doc:`pypy3 <TestSuites/pypy3>`

* :doc:`python2 <TestSuites/python2>`

* :doc:`qemu <TestSuites/qemu>`

* :doc:`rails <TestSuites/rails>`

* :doc:`simplesamlphp <TestSuites/simplesamlphp>`

-------
Stretch
-------

* :doc:`c-ares <TestSuites/c-ares>`

* :doc:`clamav <TestSuites/clamav>`

* :doc:`curl <TestSuites/curl>`

* :doc:`emacs24 <TestSuites/emacs24>`

* `firebird* <https://lists.debian.org/debian-lts/2018/04/msg00090.html>`_

* :doc:`freerdp <TestSuites/freerdp>` [v1]

* `ghostscript <https://lists.debian.org/debian-lts/2019/03/msg00122.html>`_ (no publicly available extensive test suite)

* :doc:`horde <TestSuites/horde>`

* :doc:`jetty <TestSuites/jetty>`

* :doc:`libxstream-java <TestSuites/libxstream-java>`

* :doc:`mysql-connector-java <TestSuites/mysql-connector-java>`

* :doc:`nginx <TestSuites/nginx>`

* :doc:`openexr <TestSuites/openexr>`

* :doc:`php <TestSuites/php>`
* :doc:`python3 <TestSuites/python3>`

* :doc:`request-tracker4 <TestSuites/request-tracker4>`

* :doc:`salt <TestSuites/salt>`
* :doc:`sane-backends <TestSuites/sane-backends>`
* :doc:`subversion <TestSuites/subversion>`

* :doc:`tiff <TestSuites/tiff>`

* :doc:`uwsgi <TestSuites/uwsgi>`

* :doc:`wordpress <TestSuites/wordpress>`

* :doc:`zabbix <TestSuites/zabbix>`

------
Jessie
------

* :doc:`aspell <TestSuites/aspell>`

* :doc:`c-ares <TestSuites/c-ares>`

* :doc:`kdepim <TestSuites/kdepim>`

* :doc:`libav <TestSuites/libav>`
* :doc:`libonig <TestSuites/libonig>`

* `mariadb <https://lists.debian.org/debian-lts/2019/07/msg00049.html>`_

* :doc:`netty <TestSuites/netty>`
* :doc:`ntp <TestSuites/ntp>`

* :doc:`phpmyadmin <TestSuites/phpmyadmin>`

* :doc:`slurm-llnl <TestSuites/slurm-llnl>`
* `sqlalchemy <https://lists.debian.org/debian-lts/2019/03/msg00063.html>`_
* :doc:`sqlite3 <TestSuites/sqlite3>`
* `symfony <https://lists.debian.org/debian-lts/2019/03/msg00024.html>`_

* :doc:`tomcat <TestSuites/tomcat>`

-----
Tools
-----

* :doc:`autopkgtest <TestSuites/autopkgtest>`: running/creating/backporting DEP-8 tests

* `make-scratch-ntfs
  <https://git.spwhitton.name/dotfiles/tree/scripts/media/make-scratch-ntfs>`_:
  helpful for tests which require a case-insensitive filesystem, like some of
  git's
