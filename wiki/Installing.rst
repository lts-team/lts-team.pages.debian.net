==============
Installing
==============

**We recommend using the current stable release (currently Debian 12 "Bookworm") for new installations, but it is still possible to install Debian 11 "Bullseye" during its Long Term Support (LTS) period.**

 #. Make sure `the conditions of LTS <https://wiki.debian.org/LTS>`_ fit your system (limited architectures, several unsupported packages). Read the :doc:`FAQ`.

 #. Download the install medium for `Buster <https://www.debian.org/releases/bullseye/debian-installer/>`_.

 #. Start installing as usual. Help may be found under
     * https://www.debian.org/releases/bullseye/debian-installer/
     * https://www.debian.org/releases/bullseye/installmanual
     * `DebianInstaller/FAQ <https://wiki.debian.org/DebianInstaller/FAQ>`_
     * `DebianInstaller <https://wiki.debian.org/DebianInstaller>`_

 #. Reboot and login as root.

 #. Install security updates that depend on new packages:
    ::

     apt-get upgrade --with-new-pkgs

 #. Reboot again, and login as root.

 #. Install `debian-security-support <https://packages.debian.org/search?keywords=debian-security-support>`_.
    *check-support-status* is run as a trigger when installing packages.
    You can also run *check-support-status* manually to check if your package selection is supported. (see `Check_for_unsupported_packages in the Wiki <https://wiki.debian.org/LTS/Using#Check_for_unsupported_packages>`_ and the `man page of check-support-status(1) <http://manpages.debian.org/cgi-bin/man.cgi?query=check-support-status&sektion=1&apropos=0&manpath=Debian+testing+jessie>`_)

    ::

     apt-get install debian-security-support
     check-support-status

 #. Subscribe to the `debian-lts-announce mailinglist <https://wiki.debian.org/LTS/Contact#debian-lts-announce>`_
    to get important information about new uploads and the status of the LTS distribution.

See also `What can I do if security support for a package has ended? <https://wiki.debian.org/LTS/Using#What_can_I_do_if_security_support_for_a_package_has_ended.3F>`_ in the Wiki.
