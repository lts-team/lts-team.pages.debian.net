############
Meetings
############

This page collects details of meetings of the `LTS <https://wiki.debian.org/LTS>`_ team.
Here you can download the ics file to import the meetings in your
calendar :download:`2025.ics <../_static/meetings/2025.ics>`

.. attention::
   The jitsi.debian.social has been restored, with a requirement to authenticate through OpenID and an account on Salsa. As a result, we have returned to the original Jitsi meeting location: https://jitsi.debian.social/LTS-monthly-meeting

-----------------
Upcoming Meetings
-----------------

 * **Fourth Thursday of the month, 14 UTC**
 * The agenda is prepared at https://pad.riseup.net/p/lts-meeting-agenda
 * 2025

   * Thursday,  **February 27**, 14:00 UTC on Jitsi https://jitsi.debian.social/LTS-monthly-meeting
   * Thursday,  **March 27**, 14:00 UTC on IRC #debian-lts
   * Thursday,  **April 24**, 14:00 UTC on Jitsi https://jitsi.debian.social/LTS-monthly-meeting
   * Thursday,  **May 22**, 14:00 UTC on IRC #debian-lts
   * Thursday,  **June 26**, 14:00 UTC on Jitsi https://jitsi.debian.social/LTS-monthly-meeting
   * Thursday,  **July 24**, 14:00 UTC on IRC #debian-lts
   * Thursday,  **August 28**, 14:00 UTC on Jitsi https://jitsi.debian.social/LTS-monthly-meeting
   * Thursday,  **September 25**, 14:00 UTC on IRC #debian-lts
   * Thursday,  **October 23**, 14:00 UTC on Jitsi https://jitsi.debian.social/LTS-monthly-meeting
   * Thursday,  **November 27**, 14:00 UTC on IRC #debian-lts
   * Thursday,  **December 18**, 14:00 UTC on Jitsi https://jitsi.debian.social/LTS-monthly-meeting

-------------
Past Meetings
-------------

NB: We use `MeetBot <https://wiki.debian.org/MeetBot>`_ for our IRC meetings. You can reach the full meeting log from the summary.


 * 2025

  * `January 23rd: summary <http://meetbot.debian.net/debian-lts/2025/debian-lts.2025-01-23-14.00.html>`_

 * 2024

  * `December 19th: summary <https://lists.debian.org/debian-lts/2024/12/msg00053.html>`_
  * `November 28th: summary <http://meetbot.debian.net/debian-lts/2024/debian-lts.2024-11-28-14.00.html>`_
  * `October 24th: summary <https://lists.debian.org/debian-lts/2024/10/msg00044.html>`_
  * `September 26th: summary <http://meetbot.debian.net/debian-lts/2024/debian-lts.2024-09-26-14.00.html>`_
  * `August 22nd: summary <https://lists.debian.org/debian-lts/2024/08/msg00041.html>`_
  * `July 18th: summary <http://meetbot.debian.net/debian-lts/2024/debian-lts.2024-07-18-14.01.html>`_
  * `June 27th: summary <https://lists.debian.org/debian-lts/2024/06/msg00012.html>`_
  * `May 23rd: summary <http://meetbot.debian.net/debian-lts/2024/debian-lts.2024-05-23-14.00.html>`_
  * `April 25th: summary <https://lists.debian.org/debian-lts/2024/04/msg00113.html>`_
  * `March 28th: IRC log <https://people.debian.org/~roberto/lts_meetings/debian-lts_irc_meeting_log.20240328.txt>`_
  * `February 22nd: summary <https://lists.debian.org/debian-lts/2024/02/msg00020.html>`_
  * `January 25th: summary <http://meetbot.debian.net/debian-lts/2024/debian-lts.2024-01-25-13.58.html>`_

 * 2023

  * `December 21st: summary <https://lists.debian.org/debian-lts/2023/12/msg00033.html>`_
  * `November 30th: summary <http://meetbot.debian.net/debian-lts/2023/debian-lts.2023-11-30-13.57.html>`_
  * `October 26th: summary <https://lists.debian.org/debian-lts/2023/10/msg00026.html>`_
  * `September 28th: summary <http://meetbot.debian.net/debian-lts/2023/debian-lts.2023-09-28-13.58.html>`_
  * `August 24: Jitsi meeting, Agenda <https://jitsi.debian.social/LTS-monthly-meeting>`_
  * `July 27th: summary <http://meetbot.debian.net/debian-lts/2023/debian-lts.2023-07-27-13.59.html>`_
  * `June 22nd: Jitsi meeting, Agenda <https://pad.riseup.net/p/lts-meeting-agenda>`_
  * `May 25th: summary <http://meetbot.debian.net/debian-lts/2023/debian-lts.2023-05-25-13.58.html>`_
  * April 27th: meeting was cancelled
  * `March 23rd: summary <http://meetbot.debian.net/debian-lts/2023/debian-lts.2023-03-23-13.58.html>`_
  * `February 23rd: Jitsi meeting, Agenda <https://pad.riseup.net/p/lts-meeting-agenda>`_
  * `January 26th: summary <http://meetbot.debian.net/debian-lts/2023/debian-lts.2023-01-26-14.00.html>`_

 * 2022

  * `December 22nd: Jitsi meeting, Agenda <https://pad.riseup.net/p/lts-meeting-agenda>`_
  * `November 24th: summary <http://meetbot.debian.net/debian-lts/2022/debian-lts.2022-11-24-13.59.html>`_
  * `October 27th: Jitsi meeting, Agenda <https://pad.riseup.net/p/lts-meeting-agenda>`_
  * `September 22th: summary <http://meetbot.debian.net/debian-lts/2022/debian-lts.2022-09-22-13.58.html>`_
  * `August 25th: Jitsi meeting, Agenda <https://pad.riseup.net/p/lts-meeting-agenda>`_
  * `July 28th: summary <http://meetbot.debian.net/debian-lts/2022/debian-lts.2022-07-28-14.01.html>`_
  * `June 23th: Jitsi meeting, Agenda <https://pad.riseup.net/p/lts-meeting-agenda>`_
  * May 26th, 14 UTC on #debian-lts on irc.debian.org, cancelled
  * `April 28th: Jitsi meeting, Agenda <https://pad.riseup.net/p/lts-meeting-agenda>`_
  * `March 24th: summary <http://meetbot.debian.net/debian-lts/2022/debian-lts.2022-03-24-14.00.html>`_
  * `February 24th: Jitsi meeting, Agenda <https://pad.riseup.net/p/lts-meeting-agenda>`_
  * `January 27th: summary <http://meetbot.debian.net/debian-lts/2022/debian-lts.2022-01-27-14.00.html>`_

 * 2021

  * `MeetBot log archive for 2021 <http://meetbot.debian.net/debian-lts/2021/>`_
  * `December: Video meeting, Agenda <https://pad.riseup.net/p/lts-meeting-agenda>`_
  * `November: summary <http://meetbot.debian.net/debian-lts/2021/debian-lts.2021-11-25-14.00.html>`_
  * `October: Video meeting, Agenda <https://pad.riseup.net/p/lts-meeting-agenda>`_
  * `September: summary <http://meetbot.debian.net/debian-lts/2021/debian-lts.2021-09-30-14.01.html>`_
  * `August: Video meeting, Agenda <https://pad.riseup.net/p/lts-meeting-agenda>`_
    * `DebConf21 BoF <https://debconf21.debconf.org/talks/103-funding-projects-to-improve-debian/>`_
  * `July: summary <http://meetbot.debian.net/debian-lts/2021/debian-lts.2021-07-29-14.09.html>`_
  * June: No data
  * `May: Cancelled, summary <http://meetbot.debian.net/debian-lts/2021/debian-lts.2021-05-27-15.12.html>`_
  * `April: Video meeting notes <https://wiki.debian.org/LTS/Meetings?action=AttachFile&do=view&target=lts_meeting_April2021.txt>`_
  * `March: summary <http://meetbot.debian.net/debian-lts/2021/debian-lts.2021-03-25-14.58.html>`_
  * `February: Video meeting, Agenda <https://pad.riseup.net/p/lts-meeting-agenda>`_
  * `January: summary <http://meetbot.debian.net/debian-lts/2021/debian-lts.2021-01-28-14.58.html>`_

 * 2020

  * `MeetBot log archive for 2020 <http://meetbot.debian.net/debian-lts/2020/>`_
  * DebConf20 BoF `talk <https://debconf20.debconf.org/talks/72-debian-lts-bof/>`_
  * DebConf20 BoF `videconf <https://meetings-archive.debian.net/pub/debian-meetings/2020/DebConf20/72-debian-lts-bof.webm>`_

 * 2016

  * `DebConf 16 BoF <https://lists.debian.org/debian-lts/2016/07/msg00173.html>`_

 * Debian Security Team Meeting Feb 2014 in Essen/Germany

  * `Bits from the Security Team <https://lists.debian.org/debian-devel-announce/2014/03/msg00004.html>`_ LTS notes are near the end of the text
  * `Live notes from the meeting <https://wiki.debian.org/LTS/Meetings?action=AttachFile&do=view&target=secteamessen2014.html>`_ notes about LTS are near the end

