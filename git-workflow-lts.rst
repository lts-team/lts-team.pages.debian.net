
Git workflow for the (E)LTS packages
====================================

Rationale
---------

(E)LTS team manages several releases and many packages. To keep the history of all uploads the git-workflow is mandatory.
All packages are hosted in `this project <https://salsa.debian.org/lts-team/packages/>`_ on salsa.debian.org.

For each package we have a dedicated repository. Our preference is to do the
work in the maintainer repository (assuming that the maintainer has a
`DEP-14 <https://dep-team.pages.debian.net/deps/dep14/>`_
layout and is willing to have LTS/ELTS work in the same repository). If that is
not possible, then our next preference is to fork the project in Salsa, under
the aforementioned ``lts-team/packages`` group. Only if neither working in the
maintainer repo nor forking an existing Salsa repo is possible, do we want to
proceed with creating an entirely new project under ``lts-team/packages``.
Regardless of whether the work is done in the maintainer repo or in a repo under
``lts-team/packages`` (or both in the case of LTS in the maintainer repo and
ELTS in a fork or separate repo), the repo location(s) must be listed in
the package DB.

Note that the package DB and the ``package-operations`` script described below
are in a repository which is only available to paid LTS contributors. If you do
not have access to this repository but need to perform some of these actions,
please mail the `LTS mailing list <https://lists.debian.org/debian-lts>`_ and
request assistance.

Each package must have a working CI (continuous integration) pipeline. An exception can be granted to some large or difficult
packages where the CI is difficult to setup or is impossible for some technical reason. Each
exception should be discussed on the `LTS mailing list`_.

This document describes the steps needed for the new repository creation, setting up a CI system
and preparation of an upload.

The following steps for the repo creation were automated in the  ``package-operations`` script from
``gitlab.com:freexian-lts/debian-lts``. The repository can be created during the package addition
to the dla-needed.txt or with the command ``bin/package-operation --creategitrepo --package $packagename``.
Be sure to have a created token as described `here <https://lts-team.pages.debian.net/wiki/Development.html#front-desk-duties>`_.

Fork an existing repository
---------------------------

If working in the maintainer repo is not possible, then the next thing to try is
forking the repo. This assumes that the maintainer repo already exists in Salsa
somewhere. Repositories should be forked under the ``lts-team/packages`` group.
If you lack sufficient permissions for this, contact the `LTS mailing list`_ for
help.

Prepare a new repository
------------------------

.. attention::
   A new repository should only be created if neither working in the maintainer
   repo nor forking the existing Salsa repo are possible.

.. note::
   Please check whether an original package is using git-workflow/Salsa-CI already. If so, it is preferred to fork the maintainer's existing repo to reduce resource consumption on Salsa.


As an example, the package ``admesh`` for the LTS upload into ``debian/bullseye`` will be used. You will of course have to use your own package name and corresponding release.


#. First check whether the repository for this package exists. If yes; skip these steps and go to the *Prepare an upload...* section.
#. Go to the `main repo <https://salsa.debian.org/lts-team/packages>`_ and press "New project" -> "`Create blank project <https://salsa.debian.org/projects/new?namespace_id=6008#blank_project>`_".
#. For the "Project name" field, enter ``admesh``.
#. Press "Create project".

We are using the `DEP-14 <https://dep-team.pages.debian.net/deps/dep14/>`_ schema, where the branches are named according to the releases, such as ``debian/bullseye``\ , ``debian/buster``\ , etc.

We drop the ``main`` branch and create corresponding branches.


#. Go to "Code->branches" and press "New branch".
#. Branch name -> ``debian/bullseye`` or any other suite, which is needed for your work now.
#. Go to  "Settings->Repository->Default Branch" and choose ``debian/bullseye``->"Save changes".
#. Go to "Repository->Branches" and remove ``main`` branch.

Setting up a CI
^^^^^^^^^^^^^^^


#. Go to "Settings->CI/CD". Expand "General pipelines":

   #. Enter in "CI/CD configuration file": **debian/salsa-ci.yml**
   #. Enter in "Timeout": **6h** (note: this is capped by the Runner's
      timeout, current 3h, and `SALSA_CI_BUILD_TIMEOUT_ARGS
      <https://salsa.debian.org/lts-team/pipeline/-/blob/master/salsa-ci.yml>`_,
      currently 2.75h)
   #. Press "Save changes".

#. Expand "Runners". Ensure there's an **ARM runner** enabled: LTS
   Team ARM runner and/or salsaci-arm64-runner*.

Import of new version of the package
------------------------------------

Assume, you have a folder "orig" where the current version of the package is download and uncompressed.

.. code-block::

   ls -l ../orig/

   drwxr-xr-x    - anton 16 May 22:36 admesh-0.98.2
   .rw-r--r-- 5.6k anton 16 May 22:36 admesh_0.98.2-3.debian.tar.xz
   .rw-r--r-- 2.1k anton 16 May 22:36 admesh_0.98.2-3.dsc
   .rw-r--r--  34k anton 16 May 22:36 admesh_0.98.2.orig.tar.xz




#. 
   In the terminal locally clone firstly an empty Repository.

   .. code-block::

       git clone git@salsa.debian.org:lts-team/packages/admesh.git
       cd admesh
       rm README.md
       git rm README.md
       git commit -am'Remove README.md'

#. 
   Create upstream branch and import the source:

   .. code-block::

       git checkout -b upstream
       gbp import-orig --debian-branch=debian/bullseye --pristine-tar  ../orig/admesh_0.98.2.orig.tar.xz

#. 
   Import initial debian-folder:

   .. code-block::

       git checkout debian/bullseye
       cp -r ../orig/admesh-0.98.2/debian .
       git add debian
       git commit -am'Initial import of debian/ folder'

#. 
   Delete ``debian/gbp.conf`` if it exists. We do this in a separate commit to record it in Git history.

   .. code-block::

       rm debian/gbp.conf && git commit -am 'Remove debian/gbp.conf'

#. 
   Tag the initial upload:

   .. code-block::

       gbp buildpackage --git-tag-only --git-sign-tags --git-debian-branch=debian/bullseye

Add salsa-ci.yml
^^^^^^^^^^^^^^^^^


#. 
   Create the file **debian/salsa-ci.yml** with the following content:

   .. code-block::

       ---
       # LTS/ELTS CI
       
       include:
       - https://salsa.debian.org/lts-team/pipeline/raw/master/recipes/bullseye.yml
       
       # These didn't work before LTS, not attempting to fix after freeze
       #blhc:
       #  allow_failure: true


   The full documentation for the Debian pipeline `may be found on the lts-team/pipeline repository <https://salsa.debian.org/lts-team/pipeline>`_.

#. 
   Commit and push the file:

   .. code-block::

       git add debian/salsa-ci.yml
       git commit -am'Add debian/salsa-ci.yml'
       git push --all
       git push --tags

If all went well, you can go to the salsa on the `package page <https://salsa.debian.org/lts-team/packages/admesh/-/pipelines>`_\ , section "CI/CD->Pipelines" and check whether the package is being built successfully or not.


.. image:: /_static/git-workflow-lts/CI-successfull.png
   :target: /_static/git-workflow-lts/CI-successfull.png
   :alt: A test image


As you see in our ``salsa-ci.yml`` file, we disable lintian and reprotest jobs because they are less relevant for the minimal security updates. Those options can be `switched on explicitly <https://salsa.debian.org/lts-team/packages/admesh/-/commit/386a0219508d5cd50f32fc33922a1d63934f2b4f>`_. Lintian is also disabled due to the fact that the newer version is used for older releases and it sometimes produces some errors.

Prepare an upload of the already existing package
-------------------------------------------------


#. 
   Clone an existing repository:

   .. code-block::

       git clone git@salsa.debian.org:lts-team/packages/admesh.git

#. 
   Hack on the package

#. 
   Try to build locally:

   .. code-block::

       gbp buildpackage --git-debian-branch=debian/bullseye

#. 
   After ``git push`` you can check the `status of the package <https://salsa.debian.org/lts-team/packages/admesh/-/pipelines>`_

#. 
   If the build was successful, the package should be tagged:

   .. code-block::

       gbp buildpackage --git-tag-only --git-sign-tags --git-debian-branch=debian/bullseye
       git push --tags
